<?php

/* Options page for Solamar Custom Plugin
 ***************************************************/

class CustomSolamarPluginSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_solamar_options_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_solamar_options_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Solamar Plugin Settings', 
            'manage_options', 
            'solamar-plugin-options-admin', 
            array( $this, 'create_solamar_plugin_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_solamar_plugin_admin_page()
    {
        // Set class property
        $this->options = get_option( 'solamar_plugin_option_name_fbog' );
        ?>
        <div class="wrap">
            <h2>Solamar Plugin Options</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'solamar_plugin_option_group' );   
                do_settings_sections( 'solamar-plugin-options-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'solamar_plugin_option_group', // Option group
            'solamar_plugin_option_name_fbog', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'solamar_plugin_setting_section_id', // ID
            'Solamar Custom Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'solamar-plugin-options-admin' // Page
        );  

        /* text field examples ...

        add_settings_field(
            'id_number', // ID
            'ID Number', // Title 
            array( $this, 'id_number_callback' ), // Callback
            'solamar-plugin-options-admin', // Page
            'solamar_plugin_setting_section_id' // Section           
        );      

        add_settings_field(
            'title', 
            'Title', 
            array( $this, 'title_callback' ), 
            'solamar-plugin-options-admin', 
            'solamar_plugin_setting_section_id'
        );      
        */

        add_settings_field(
            'fbog_check', // ID
            'Facebook Open Graph meta fields', // Title 
            array( $this, 'fbog_check_callback' ), // Callback
            'solamar-plugin-options-admin', // Page
            'solamar_plugin_setting_section_id' // Section           
        );      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();

        /* text field examples...

        if( isset( $input['id_number'] ) )
            $new_input['id_number'] = absint( $input['id_number'] );

        if( isset( $input['title'] ) )
            $new_input['title'] = sanitize_text_field( $input['title'] );

        */

        if( isset( $input['fbog_check'] )  ) 
            $new_input['fbog_check'] = $input['fbog_check'];

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /** 
     * Get the settings option array and print one of its values
    public function id_number_callback()
    {
        printf(
            '<input type="text" id="id_number" name="solamar_plugin_option_name_fbog[id_number]" value="%s" />',
            isset( $this->options['id_number'] ) ? esc_attr( $this->options['id_number']) : ''
        );
    }
     */

    /** 
     * Get the settings option array and print one of its values
    public function title_callback()
    {
        printf(
            '<input type="text" id="title" name="solamar_plugin_option_name_fbog[title]" value="%s" />',
            isset( $this->options['title'] ) ? esc_attr( $this->options['title']) : ''
        );
    }
     */

    /** 
     * Get the settings option array and print one of its values
     */
    public function fbog_check_callback()
    {
      $check = get_option( 'solamar_plugin_option_name_fbog[fbog_check]' );
      printf(
          //'<input name="solamar_plugin_option_name_fbog[fbog_check]" id="fbog_check" type="checkbox" value="' . $check . '" class="code" ' . checked( $check , 1 ) . ' /><br>- Check this to make use of Solamar\'s FB Open Graph meta fields.<br>- Uncheck it if you have another plugin adding the FB OG meta fields.',
          '<input name="solamar_plugin_option_name_fbog[fbog_check]" id="fbog_check" type="checkbox" value="%1$s" class="code" %2$s /><br>
          - Check this to make use of Solamar\'s FB Open Graph meta fields.<br>
          - Uncheck it if you have another plugin adding the FB OG meta fields.
          <style>.solalist ul { margin-left:20px; }</style>
          <h3>Facebook Open Graph meta tags are set as follows:</h3>
          <ul class="solalist">
          <li><strong>OG:IMAGE</strong>
            <ul>
              <li>First we look for the featured image of a post/page</li>
              <li>Second we load up all the images inside a post plus the site logo</li>
              <li>Lastly if there are no featured image or images within the content of the post, we just toss up the site logo</li>
            </ul>
          </li>
          <li><strong>OG:URL</strong>
            <ul>
              <li>The page permalink</li>
            </ul>
          </li>
          <li><strong>OG:TITLE</strong>
            <ul>
              <li>The page title</li>
            </ul>
          </li>
          <li><strong>OG:SITE_NAME</strong>
            <ul>
              <li>The Site Title</li>
            </ul>
          </li>
          <li><strong>OG:DESCRIPTION</strong>
            <ul>
              <li>The page description can be generated in the following manners and order
                <ol>
                  <li>Create a custom field called \'page-description\' and add the specific value you\'d like</li>
                  <li>The page specific excerpt</li>
                  <li>The page specific content</li>
                  <li>The generic site Description</li>
                </ol>
              </li>
            </ul>
          </li>
          </ul>',
            checked( isset( $this->options['fbog_check'] ), true, false ) != '' ? 1 : 0,
            checked( isset( $this->options['fbog_check'] ), true, false )
        );
    }

}

if( is_admin() )
    $my_settings_page = new CustomSolamarPluginSettingsPage();
?>
