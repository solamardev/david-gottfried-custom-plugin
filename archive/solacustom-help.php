<?php
/**
 * Solamar Custom Plugin Admin Page
 */

if ( ! current_user_can( 'manage_options' ) )
	wp_die( __( 'You do not have sufficient permissions to manage options for this site.' ) );

$title = __('Solamar Custom Plugin Settings');
$parent_file = 'options-general.php';

?>
<div class="wrap">
<?php screen_icon('plugins'); ?>
<h2><?php echo esc_html( $title ); ?></h2>
<h3>Documentation</h3>
<a href="/wp-content/themes/solamar/inc/docs/twit/docs/index.html" target="_blank">Bootstrap Docs</a>
</div>
