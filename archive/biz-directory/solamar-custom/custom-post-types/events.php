<?php

/*
 * Register_custom_post_type - Events 
 * - this custom post type is mainly utilized for when you need to have a event of content embedded
 *   within a page template somewhere, and need a place to manage it from. This allows us to not
 *   put the content inside of a regular Post and have it lost when the blog fills up.
 *************************************************/

add_action( 'init', 'create_event_post_type' );

function create_event_post_type() {

  $icon = plugins_url( 'images/script-code-single.png', __FILE__ ); 

  // create the custom post type
  register_post_type( 'event',
    array(
      'labels' => array(
        'name' => __( 'Events' ), 
        'singular_name' => __( 'Event' ), 
        'add_new' => _x('Add New', 'event'), 
        'add_new_item' => __('Add New Event'), 
        'edit_item' => __('Edit Event'), 
        'new_item' => __('New Event'), 
        'view_item' => __('View Event'), 
        'search_items' => __('Search Events'), 
        'not_found' =>  __('No events found'), 
        'not_found_in_trash' => __('No events found in Trash'), 
        'parent_item_colon' => '', 
        'menu_name' => 'Events'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true, 
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true, 
      'hierarchical' => true,
      'menu_position' => 20, // tosses the menu just below Pages and above Comments
      'menu_icon' => $icon,
      'supports' => array( 'title', 'author', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'page-attributes')
    )
  );

  // create categories for the custom post type
  register_taxonomy(
    "events", 
    array("event"), 
    array(
      "hierarchical" => true, 
      "labels" => array(
        'name' => _x( 'Event Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'Event Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Event Categories' ),
        'popular_items' => __( 'Popular Event Categories' ),
        'all_items' => __( 'All Event Categories' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Event Category' ), 
        'update_item' => __( 'Update Event Category' ),
        'add_new_item' => __( 'Add New Event Category' ),
        'new_item_name' => __( 'New Event Category Name' ),
        'separate_items_with_commas' => __( 'Separate event categories with commas' ),
        'add_or_remove_items' => __( 'Add or remove event category' ),
        'choose_from_most_used' => __( 'Choose from the most used event categories' ),
        'menu_name' => __( 'Event Categories' ),
      ),
      "show_ui" => true, 
      "query_var" => true, 
      'rewrite' => array( 'slug' => 'events', 'with_front' => true, 'heirarchical' => true )
    )
  );
}

function insert_events() {

  $events =  get_posts(
    array(
      'post_type' => 'event',
      'order' => 'ASC',
      'orderby' => 'title',
      'posts_per_page' => '-1'
    )
  );

  $output = '';

  if ( $events != NULL ) { // check to see if an object was created successfully above

    foreach ( $events as $loop ) {

      $post_id = $loop->ID;
  
      $output .= '<h1 class="entry-title">' . $loop->post_title . '</h1>';
      $output .= $loop->post_excerpt;
      $output .= '<br><br>'; 
      $output .= '<a href="' . get_permalink( $post_id ) . '">Read More &rarr;</a>';
      $output .= '<hr>';

    }

    return $output;

  } else {

    return $output;

  }
}

?>
