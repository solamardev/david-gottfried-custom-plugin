<?php
/**
 * The Template for displaying all single vendors 
 *
 * @package WordPress
 * @subpackage Solamar
 * @since Solamar 2.1
 */

get_header(); ?>

    <style>
      table { }
        table tr { border-top:1px solid #000; }
          table tr th { text-align:center; }
          table tr td { font-size: 13px; padding: 10px 10px;}
    </style>

		<div id="primary" style="padding:20px;">
			<div id="content" role="main">

        <table id="solamar-vendors">
          <!--thead>
          <tr>
            <th>Logo</th>
            <th>Title</th>
            <th width="50">Phone</th>
            <th>URL</th>
          </tr>
          </thead-->
          <tbody>

				<?php while ( have_posts() ) : the_post(); 

/* PULL THE DATA
 ******************************************************/

          $post_id = get_the_ID(); 

          $vendor_title = get_post_meta( $post_id, '_vendor_title', TRUE );
          $vendor_phone = get_post_meta( $post_id, '_vendor_phone', TRUE );
          $vendor_url = get_post_meta( $post_id, '_vendor_url', TRUE );

          $thumb = '';
          $size = 'full';
          if ( has_post_thumbnail( $post_id ) ) {
            $thumb = get_the_post_thumbnail( $post_id, $size );
          }

?>

          <tr>
            <td><?php echo $thumb; ?></td>
            <td><? echo $vendor_title; ?></td>
            <td><? echo $vendor_phone; ?></td>
            <td><? echo $vendor_url; ?></td>
          </tr>

				<?php endwhile; /* end of the loop. */ ?>

          <tbody>
        </table>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_footer(); ?>
