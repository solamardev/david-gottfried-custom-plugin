<?php
/**
 * The Template for displaying all vendors 
Template Name: Business Finder
 *
 * @package WordPress
 * @subpackage Solamar
 * @since Solamar 2.1
 */

get_header(); ?>

<style>
table { }
table tr { border-top:1px solid #000; padding: 10px 10px; }
table tr th { text-align:center; }
table tr td { font-size: 16px; padding: 10px 10px;}
</style>

		<div id="primary" style="padding:20px;">
			<div id="content" role="main">

				<form class="art-search" method="get" name="searchform" action="<?php bloginfo('url'); ?>/">
				<input type="hidden" name="post_type" value="vendor" />
					<input name="s" type="text" value="<?php echo esc_attr(get_search_query()); ?>" />
					<input class="art-search-button" type="submit" value="<?php echo __('Search', THEME_NS); ?>" />
				</form>

				<?php echo insert_vendor_categories(); ?>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_footer(); ?>
