<?php
/**
 * The Template for displaying the current DJ 
 *
 * @package WordPress
 * @subpackage Solamar
 * @since Solamar 2.1
 */

get_header(); ?>


		<div id="primary" style="padding:20px;">
			<div id="content" role="main">

				<?php echo insert_current_dj(); ?>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_footer(); ?>
