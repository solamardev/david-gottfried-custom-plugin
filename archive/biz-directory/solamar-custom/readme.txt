=== Solamar Custom Plugin ===
Contributors: Ben Kaplan
Tags: testimonials, credits, reviews 
Requires at least: 3.0
Tested up to: 3.3.1
Stable tag: 1.2.1.1

Solamar Custom Plugin lets you do all sorts of fun stuff :)

=== Description ===

Solamar Custom Plugin lets you do all sorts of fun stuff :)

Major new features in Solamar Testimonials include:

* added ability to click on links and directly download a file
* add/remove dummy content to site

=== Installation ===

Upload the Solamar plugin to your blog, and activate it.

=== Changelog ===

= 1.2.1.1 =
[+] added <link> tag for FB image in header.php
[*] standardized social media solafonts

= 1.2.1 =
[*] added download.php to package

= 1.2 =
[*] added dummy content functionality

= 1.1 = 
[+] Add controls for length of time and length of transition to widget
[*] Slight adjustments to CSS to make styles more generic

= 1.0 =
[*] Base plugin with custom post type, shortcode, and widget.
