<?php
/*
* Plugin Name: Solamar Custom Functionality 
* Plugin URI: http://www.solamarmarketing.com/
* Description: A plugin that adds functionality specifically design WJLD - specifically the Business directory 
* Version: 1.2 
* Author: Ben Kaplan 
* Author URI: http://www.benzot.net/

* General stuff
*
* Register_custom_post_type 
* Register a custom post type, 'bits' 
*
*/

$plugin_path = plugin_dir_path( dirname(__FILE__) );

/*
 *
 * General Stuff
 *************************************************/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
	exit;
}

/*
 * Add dummy content to site for development
 *************************************************/
//require( $plugin_path . 'solamar-custom/dummy-content/magic.php' );

/*
 * Register_custom_post_type 
 *************************************************/
require( $plugin_path . 'solamar-custom/custom-post-types/dj.php' );
require( $plugin_path . 'solamar-custom/custom-post-types/vendor.php' );
//require( $plugin_path . 'solamar-custom/custom-post-types/events.php' );
//require( $plugin_path . 'solamar-custom/custom-post-types/sponsors.php' );
//require( $plugin_path . 'solamar-custom/custom-post-types/slider.php' );

/*
 * custom shortcode for inserting custom fields 
 *************************************************/
require( $plugin_path . 'solamar-custom/function/shortcodes.php' );
//require( $plugin_path . 'solamar-custom/function/insert-slider.php' );

/*
 * Add a new page to the menu for Documentation 
 *
 *****************************************************/

//add_action('admin_menu' , 'solamar_solatheme_enable_pages');
 
function solamar_solatheme_enable_pages() {
 add_menu_page('Solamar Custom Plugin', 'SolaCustom', 'administrator', 'solamar-custom/solacustom-help.php', '', plugins_url('/solamar-custom/custom-post-types/images/script-code.png'), 99.1);
}


?>
