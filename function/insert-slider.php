<?php
/* 
 * insert a nice bootstrap slideshow of some passed custom post type 
 ******************************************************************/

function insert_solamar_slideshow( $custom_post_type = '', $custom_post_type_category = '', $custom_post_type_category_value = '' ) {

  // check to see if the custom post type was passed.  If not, throw a message back asking for one
  if ( $custom_post_type != '' ) {

    $cpost = $custom_post_type;

    $the_loop;

    // check to see if a category is passed.  If so, run a query with taxonomy, else run without
    if ( $custom_post_type_category != '' ) {
      
      $cpost_category = $custom_post_type_category;

      $cpost_category_value = $custom_post_type_category_value;

      // pull a new wp_query object based upon the Project Category
      $the_loop = get_posts( array( 'post_type' => $cpost, $cpost_category => $cpost_category_value, 'order' => 'ASC', 'orderby' => 'menu_order', 'posts_per_page' => -1 ) );

    } else {

      // pull a new wp_query object based upon the Project Category
      $the_loop = get_posts( array( 'post_type' => $cpost, 'order' => 'ASC', 'orderby' => 'menu_order', 'posts_per_page' => -1 ) );

    }

    if ( !empty( $the_loop ) ) { 

      $output;

      $output .= '<div id="myCarousel" class="carousel slide">';
      $output .= '<!-- Carousel items -->';
      $output .= '<div class="carousel-inner">';


      $i = 1;

      foreach ( $the_loop as $item ) { 

        $post_id = $item->ID;
        $size = 'full';
        $active = '';
        $cpost_title = $item->post_title;;
        $cpost_content = apply_filters('the_content', $item->post_content );

        if ( $i == 1 ) {
          $active = 'active '; 
        }

        // pull the attachments from the Project.  These will be used to populate the Fancybox Overlay
        if ( has_post_thumbnail($post_id) ) {

          $the_featured_image = get_the_post_thumbnail( $post_id, $size );
          $post_link = get_permalink($post_id);
          
          $output .= '<div class="' . $active . 'item">'; 
          $output .= '  <a href="' . $post_link . '">' . $the_featured_image . '</a>'; 
          $output .= '  <div class="carousel-caption">';
          $output .= '    <h4>' . $cpost_title . '</h4>';
          $output .= '    <div class="carousel-content">' . $cpost_content . '</div>';
          $output .= '  </div>';
          $output .= '</div>';

        }

        $i++;

      } 

      $output .= '</div>';
      $output .= '<!-- Carousel nav -->';
      $output .= '<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>';
      $output .= '<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>';
      $output .= '</div>';

      return $output;

    } else {

      $output = 'Please pass me a custom post type that has something in it!';

      return $output;

    }

  } else {

    $output = 'Please pass me a custom post type!';

    return $output;

  } // end if ( $custom_post_type != null ) 

} // end insert_gallery()

// let's make this a short code for insertion into any page template.
add_shortcode( 'solamar-slideshow', 'insert_solamar_slideshow' );


?>
