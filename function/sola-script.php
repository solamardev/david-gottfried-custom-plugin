<?php
/**
  * Inserts the value of the custom field 'solascript'.  
  * Presumably one will only add <script> tags here as this is output to the <head></head> tag.
  * @post - requires the global post
  */
function insert_solascript( $post ) { 

  if ( $post != NULL ) { 

    $post_id = $post->ID;

    $script = get_post_meta( $post_id, 'solascript', true );

    echo $script;

  }

}
?>
