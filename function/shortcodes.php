<?php
/*
 * field_func() 
 * Allows display of custom fields via shortcode 
 * USAGE:
 *  create custom field
 *  insert shortcode [field name=custom-field-name]
 *************************************************/
function field_func($atts) {

  global $post;

  $name = $atts['name'];

  if (empty($name)) return;

  return get_post_meta($post->ID, $name, true);

}

add_shortcode('field', 'field_func');

?>
