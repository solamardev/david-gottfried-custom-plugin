<?php

function catch_first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches[1][0];

  if(empty($first_img)) {
    $first_img = "/path/to/default.png";
  }
  return $first_img;
}

function catch_all_images($post) {
  $htmlContent = $post->post_content;

  // read all image tags into an array
  $output = preg_match_all('/<img[^>]+>/i',$htmlContent, $imgTags); 

  for ($i = 0; $i < count($imgTags[0]); $i++) {
    // get the source string
    preg_match('/src="([^"]+)/i',$imgTags[0][$i], $imgage);

    // remove opening 'src=' tag, can`t get the regex right
    $origImageSrc[] = str_ireplace( 'src="', '',  $imgage[0]);
  }
  // will output all your img src's within the html string
  if ( $output == 0 || $output == false ) {
    return false;
  } else {
    return $origImageSrc;
  }
}

?>
