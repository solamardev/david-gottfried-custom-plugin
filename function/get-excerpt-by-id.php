<?php
/* 
 * Create an excerpt outside of the loop
 **********************************************/
function get_excerpt_by_id($post_id){

  // Gets post ID
  $the_post = get_post($post_id); 

  // Gets post_content to be used as a basis for the excerpt
  $the_excerpt = $the_post->post_content; 

  // Sets excerpt length by word count
  $excerpt_length = 35; 

  // clean up the content to remove all html and shortcodes
  $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images

  $words = explode(' ', $the_excerpt, $excerpt_length + 1);

  if(count($words) > $excerpt_length) :

    array_pop($words);

    array_push($words, '…');

    $the_excerpt = implode(' ', $words);

  endif;

  $the_excerpt = '<p>' . $the_excerpt . '<br><a class="read-more" href="'. get_permalink( $post_id ) . '">' . ' Read more &raquo;' . '</a></p>';

  return $the_excerpt;
}
?>
