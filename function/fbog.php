<?php
/*
 * insert_fbog()
 * outputs Facebook Open Graph Meta tags
 * sets: 
 *   og:type - 'website'
 *   og:url - permalink
 *   og:title - 'title of page'
 *   og:image - 'featured image w/ logo OR images in content w/ logo OR logo'
 *   og:site_name - 'site title'
 *   og:description - 'custom field if set, excerpt if set, site description'
 ****************************************************************************************/

function insert_fbog( $post ) {

  $show_fbog = get_option( 'solamar_plugin_option_name_fbog' );

  $output = '';

  if ($post && !empty($show_fbog) ) {

    $blurb = '';
    $the_thumb = '';
    $post_id = $post->ID;

    $images = catch_all_images($post);
    $og_images = '';

    if ( has_post_thumbnail() ) { // if there is a thumbnail

      // let's toss in the thumbnail
      $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      $size = 'full';
      $the_thumb_array = wp_get_attachment_image_src( $post_thumbnail_id , $size );
      $the_thumb = $the_thumb_array[0];
      $og_images .= '<meta content="' . $the_thumb . '" property="og:image" />';

      // let's go ahead and toss the site logo in as an option
      $the_thumb = get_bloginfo('stylesheet_directory') . '/images/fb.png';
      $og_images .= '<meta content="' . $the_thumb . '" property="og:image" />';


    } elseif ( $images ) { // if there are images inside the content of the post and NO featured image
      
      // let's toss in all the images found inside the post content
      foreach ( $images as $img_src ) {
        $og_images .= '<meta content="' . get_bloginfo('url') . $img_src . '" property="og:image" />';
      }

      // let's go ahead and toss the site logo in as an option
      $the_thumb = get_bloginfo('stylesheet_directory') . '/images/fb.png';
      $og_images .= '<meta content="' . $the_thumb . '" property="og:image" />';

    } else { // if there is no featured image AND there are no images in the post content

      // let's toss in the site logo
      $the_thumb = get_bloginfo('stylesheet_directory') . '/images/fb.png';
      $og_images .= '<meta content="' . $the_thumb . '" property="og:image" />';

    }

    // pull custom meta
    $post_custom_fields = get_post_custom($post_id);

    // an empty variable
    $my_custom_field = ''; 

    // test for custom field 'page-description'
    if ( isset($post_custom_fields['page-description']) ) {
      $my_custom_field = $post_custom_fields['page-description'];

      // let's just make sure that the custom field 'page-description' isn't set but empty
      if ( $my_custom_field[0] != '' ) { 
        $blurb = $my_custom_field[0];

      // check for a post excerpt
      } elseif ( has_excerpt( $post_id ) ) {
        $blurb = get_the_excerpt();

      // otherwise, just return the site description
      } else {
        $blurb = get_bloginfo( 'description' );
      }

    // if no custom field, then test for excerpt
    } elseif ( has_excerpt( $post_id ) ) {
      $blurb = get_the_excerpt();

    // otherwise, just return the first 35 words 
    } else {
      $blurb = my_sola_excerpt($post->post_content, get_the_excerpt());
    }

    $blurb = wp_strip_all_tags($blurb);

    $url = get_bloginfo('wpurl');;

    if ( is_home() || is_front_page() ) { 
      
      // prevents Index of blog posts from returning blog post url rather than home page
      $url = get_bloginfo('wpurl');;

    } else {

      $url = get_permalink($post_id);

    }

    $output .= '<meta content="website" property="og:type" />';
    $output .= '<meta content="' . $url  . '" property="og:url" />';
    $output .= '<meta content="' . get_the_title($post_id) . '" property="og:title" />';
    $output .= $og_images;
    $output .= '<meta content="' . get_bloginfo('name') . '" property="og:site_name" />';
    $output .= '<meta content="' . $blurb . '" property="og:description" />';

  }

  return $output;

}

function my_sola_excerpt($text, $excerpt) {

  if ($excerpt) return $excerpt;

  $text = strip_shortcodes( $text );

  $text = apply_filters('the_content', $text);

  $text = str_replace(']]>', ']]&gt;', $text);

  $text = strip_tags($text);

  $excerpt_length = apply_filters('excerpt_length', 35);

  $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');

  $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);

  if ( count($words) > $excerpt_length ) { 
    array_pop($words);
    $text = implode(' ', $words);
    $text = $text . $excerpt_more;
  } else {
    $text = implode(' ', $words);
  }

  return apply_filters('wp_trim_excerpt', $text, $excerpt);
}


?>
