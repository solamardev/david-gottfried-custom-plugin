<?php
/*
* Plugin Name: Solamar Custom Functionality 
* Plugin URI: http://www.solamarmarketing.com/
* Description: A plugin that adds functionality specifically design for the Solamar theme 
* Version: 1.2 
* Author: Ben Kaplan 
* Author URI: http://www.benzot.net/

* General stuff
*
* Register_custom_post_type 
* Register a custom post type, 'bits' 
*
*/


/*
 *
 * Security 
 *************************************************/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
  die;
}

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
	exit;
}

/*
 *
 * General Stuff
 *************************************************/

$plugin_path = plugin_dir_path( dirname(__FILE__) );

/*
 *
 * Start Things 
 *************************************************/

require( $plugin_path . 'solamar-custom/admin/options.php' );

/*
 * Add dummy content to site for development
 *************************************************/
//require( $plugin_path . 'solamar-custom/dummy-content/magic.php' );

/*
 * Register_custom_post_type 
 *************************************************/
require( $plugin_path . 'solamar-custom/custom-post-types/bits.php' );
require( $plugin_path . 'solamar-custom/custom-post-types/people.php' );
//require( $plugin_path . 'solamar-custom/custom-post-types/slides.php' );
//require( $plugin_path . 'solamar-custom/custom-post-types/faqs.php' );

/*
 * custom functions 
 *************************************************/
require( $plugin_path . 'solamar-custom/function/general.php' );

/*
 * custom shortcode for inserting custom fields 
 *************************************************/
require( $plugin_path . 'solamar-custom/function/catch-the-images.php' );
require( $plugin_path . 'solamar-custom/function/fbog.php' );
require( $plugin_path . 'solamar-custom/function/insert-slider.php' );
require( $plugin_path . 'solamar-custom/function/get-excerpt-by-id.php' );
require( $plugin_path . 'solamar-custom/function/shortcodes.php' );
require( $plugin_path . 'solamar-custom/function/sola-script.php' );

/*
 * Add a new page to the menu for Documentation 
 *
 *****************************************************/

//add_action('admin_menu' , 'solamar_solatheme_enable_pages');
 
function solamar_solatheme_enable_pages() {
 add_menu_page('Solamar Custom Plugin', 'SolaCustom', 'administrator', 'solamar-custom/solacustom-help.php', '', plugins_url('/solamar-custom/custom-post-types/images/script-code.png'), 99.1);
}


?>
