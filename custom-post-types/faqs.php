<?php

/*
 * Register_custom_post_type - Faqs 
 *************************************************/
/*

*/

add_action( 'init', 'create_faq_post_type' );

function create_faq_post_type() {

  $icon = plugins_url( 'images/script-code-single.png', __FILE__ ); 

  // create the custom post type
  register_post_type( 'faq',
    array(
      'labels' => array(
        'name' => __( 'FAQs' ), 
        'singular_name' => __( 'FAQ' ), 
        'add_new' => _x('Add New', 'faq'), 
        'add_new_item' => __('Add New FAQ'), 
        'edit_item' => __('Edit FAQ'), 
        'new_item' => __('New FAQ'), 
        'view_item' => __('View FAQ'), 
        'search_items' => __('Search FAQs'), 
        'not_found' =>  __('No FAQs found'), 
        'not_found_in_trash' => __('No FAQs found in Trash'), 
        'parent_item_colon' => '', 
        'menu_name' => 'FAQs'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true, 
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true, 
      'hierarchical' => true,
      'menu_position' => 20, // tosses the menu just below Pages and above Comments
      'menu_icon' => $icon,
      'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'page-attributes')
    )
  );

  // create categories for the custom post type
  register_taxonomy(
    "faqs", 
    array("faq"), 
    array(
      "hierarchical" => true, 
      "labels" => array(
        'name' => _x( 'FAQ Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'FAQ Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search FAQ Categories' ),
        'popular_items' => __( 'Popular FAQ Categories' ),
        'all_items' => __( 'All FAQ Categories' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit FAQ Category' ), 
        'update_item' => __( 'Update FAQ Category' ),
        'add_new_item' => __( 'Add New FAQ Category' ),
        'new_item_name' => __( 'New FAQ Category Name' ),
        'separate_items_with_commas' => __( 'Separate FAQ categories with commas' ),
        'add_or_remove_items' => __( 'Add or remove FAQ category' ),
        'choose_from_most_used' => __( 'Choose from the most used FAQ categories' ),
        'menu_name' => __( 'FAQ Categories' ),
      ),
      "show_ui" => true, 
      "query_var" => true, 
      'rewrite' => array( 'slug' => 'faqs', 'with_front' => true, 'hierarchical' => true )
    )
  );
}

function insert_faqs( $cat = null ) {

  $category = $cat;
  
  if ( $category == null ) {
    
    $faqs = get_posts( array( 'post_type' => 'faq', 'order' => 'ASC', 'orderby' => 'menu_order', 'posts_per_page' => -1 ) );

  } else {

    $faqs = get_posts( array( 
      'post_type' => 'faq', 
      'tax_query' => array( array (
        'taxonomy' => 'faqs',
        'field' => 'slug',
        'terms' => $category
      )),
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'posts_per_page' => -1
    ) );

  }

  $output .= '<div id="accordion2-' . $category . '" class="accordion">';


  foreach( $faqs as $faq ) {
    
   	$count++;

    
    $faq_id = $faq->ID;
    
    $output .= '<div class="accordion-group">';
		$output .= '<div class="accordion-heading">';

    $output .= '<p class="question"><a class="accordion-toggle icon-plus" href="#collapse-' . $category . '-' . $count .'" data-toggle="collapse" data-parent="#accordion2-' . $category . '">';
    $output .= $faq->post_title; 
    $output .= '</a></p>'; 
    $output .= '</div>';
   
    $output .= '<div id="collapse-' . $category . '-' . $count . '" class="accordion-body collapse">';
    $output .= '<div class="accordion-inner">';
    $output .= wpautop($faq->post_content); 
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';

  }
  $output .= '</div>';


  return $output;

}
/*

SAMPLE USAGE:

In theme templates:

<?php echo insert_faqs( 'some-faq-category-slug' ); ?>

*/


/*
 * inner_faq_shortcode() 
 * Allows display of slideshows in the WYSIWYG Editor 
 * USAGE:
 *  create custom field
 *  insert shortcode [solamar-faqs cat=some-faq-category-slug]
 *************************************************/
function insert_faq_shortcode($atts) {

  global $post;

  $cat = $atts['cat'];

  if (!empty($cat)) { 

    return insert_faqs( $cat );

  } else {

    return insert_faqs();
  }

}

add_shortcode('solamar-faqs', 'insert_faq_shortcode');

?>
