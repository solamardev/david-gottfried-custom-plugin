<?php

/*
 * Register_custom_post_type - Slides 
 * creates a cusotm post type slides
 * 
 * insert_slides()
 * @cat - string - slug of slide category to show
 * returns a full bootstrap slideshow
 *
 * inner_slide_shortcode() 
 * shortcode for insertion of insert_slides() into WYSIWYG editors
 *************************************************/

add_action( 'init', 'create_slide_post_type' );

function create_slide_post_type() {

  $icon = plugins_url( 'images/script-code-single.png', __FILE__ ); 

  // create the custom post type
  register_post_type( 'slide',
    array(
      'labels' => array(
        'name' => __( 'Slides' ), 
        'singular_name' => __( 'Slide' ), 
        'add_new' => _x('Add New', 'slide'), 
        'add_new_item' => __('Add New Slide'), 
        'edit_item' => __('Edit Slide'), 
        'new_item' => __('New Slide'), 
        'view_item' => __('View Slide'), 
        'search_items' => __('Search Slides'), 
        'not_found' =>  __('No slides found'), 
        'not_found_in_trash' => __('No slides found in Trash'), 
        'parent_item_colon' => '', 
        'menu_name' => 'Slides'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true, 
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true, 
      'hierarchical' => true,
      'menu_position' => 20, // tosses the menu just below Pages and above Comments
      'menu_icon' => $icon,
      'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'page-attributes')
    )
  );

  // create categories for the custom post type
  register_taxonomy(
    "slides", 
    array("slide"), 
    array(
      "hierarchical" => true, 
      "labels" => array(
        'name' => _x( 'Slide Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'Slide Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Slide Categories' ),
        'popular_items' => __( 'Popular Slide Categories' ),
        'all_items' => __( 'All Slide Categories' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Slide Category' ), 
        'update_item' => __( 'Update Slide Category' ),
        'add_new_item' => __( 'Add New Slide Category' ),
        'new_item_name' => __( 'New Slide Category Name' ),
        'separate_items_with_commas' => __( 'Separate slide categories with commas' ),
        'add_or_remove_items' => __( 'Add or remove slide category' ),
        'choose_from_most_used' => __( 'Choose from the most used slide categories' ),
        'menu_name' => __( 'Slide Categories' ),
      ),
      "show_ui" => true, 
      'show_admin_column' => true,
      "query_var" => true, 
      'rewrite' => array( 'slug' => 'slides', 'with_front' => true, 'heirarchical' => true )
    )
  );
}

/* 
 * insert_slides()
 * @cat - string - slug of category
 * returns full twitter bootstrap slideshow
 *************************************************/

function insert_slides( $cat = null ) {

  $cat = $cat;
  
  if ( $cat == null ) {
    
    $slides = get_posts( array( 'post_type' => 'slide' ) );

  } else {

    $slides = get_posts( array( 
      'post_type' => 'slide', 
      'tax_query' => array( array (
        'taxonomy' => 'slides',
        'field' => 'slug',
        'terms' => $cat
      )),
      'posts_per_page' => -1
    ) );

  }

  $output;
  $i = 0;
  $active = '';

  $output .= '<div id="myCarousel-' . $cat . '" class="carousel slide">';
  $output .= '<div class="carousel-inner">';

  foreach( $slides as $slide ) {
    
    $slide_id = $slide->ID;
    $the_slide = get_the_post_thumbnail( $slide_id );

    if( $i == 0 ) { $active = 'active '; }
    
    $output .= '<div class="' . $active . 'item">';
    $output .= $the_slide;
    $output .= '</div>';

    if( $i == 0 ) { $active = ''; }

    $i++;

  }

  $output .= '</div><!-- END .carousel-inner -->';
  $output .= '<a class="carousel-control left" href="#myCarousel-' . $cat . '" data-slide="prev">&lsaquo;</a>';
  $output .= '<a class="carousel-control right" href="#myCarousel-' . $cat . '" data-slide="next">&rsaquo;</a>';
  $output .= '</div><!-- END #myCarousel -->';

  return $output;

}

/*

SAMPLE USAGE:

In theme templates:

<?php echo insert_slides( 'some-slide-category-slug' ); ?>

*/


/*
 * inner_slide_shortcode() 
 * Allows display of slideshows in the WYSIWYG Editor 
 * USAGE:
 *  create custom field
 *  insert shortcode [solamar-slides cat=some-slide-category-slug]
 *************************************************/
function insert_slide_shortcode($atts) {

  global $post;

  $cat = $atts['cat'];

  if (!empty($cat)) { 

    return insert_slides( $cat );

  } else {

    return insert_slides();
  }

}

add_shortcode('solamar-slides', 'insert_slide_shortcode');

/*

SAMPLE USAGE:

In a page editor use this shortcode:

// for category specific slides
[solamar-slides cat=some-slide-category-slug]

// for all slides
[solamar-slides]

*/
?>
