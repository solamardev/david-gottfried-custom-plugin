<?php

/*
 * Register_custom_post_type - Person
 * - this custom post type is mainly utilized for managing a group of people like staff for board members 
 *************************************************/
/*
 * an example of how to pull and display People 

  $a_person = new WP_Query( array('post_type' => 'person', 'pagename' => 'a-person') ); 
  if ( $a_person ) {
    while ( $a_person->have_posts() ) : $a_person->the_post();
      the_title('<h2>','</h2>');
      the_content();
    endwhile;
  }

*/

add_action( 'init', 'create_person_post_type' );

function create_person_post_type() {

  $icon = plugins_url( 'images/script-code-single.png', __FILE__ ); 

  // create the custom post type
  register_post_type( 'person',
    array(
      'labels' => array(
        'name' => __( 'People' ), 
        'singular_name' => __( 'Person' ), 
        'add_new' => _x('Add New', 'person'), 
        'add_new_item' => __('Add New Person'), 
        'edit_item' => __('Edit Person'), 
        'new_item' => __('New Person'), 
        'view_item' => __('View Person'), 
        'search_items' => __('Search People'), 
        'not_found' =>  __('No people found'), 
        'not_found_in_trash' => __('No people found in trash'), 
        'parent_item_colon' => '', 
        'menu_name' => 'People'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true, 
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true, 
      'hierarchical' => true,
      'menu_position' => 20, // tosses the menu just below Pages and above Comments
      'menu_icon' => $icon,
      'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'page-attributes')
    )
  );

  // create categories for the custom post type
  register_taxonomy(
    "people", 
    array("person"), 
    array(
      "hierarchical" => true, 
      "labels" => array(
        'name' => _x( 'People Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'People Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search People Categories' ),
        'popular_items' => __( 'Popular People Categories' ),
        'all_items' => __( 'All People Categories' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit People Category' ), 
        'update_item' => __( 'Update People Category' ),
        'add_new_item' => __( 'Add New People Category' ),
        'new_item_name' => __( 'New People Category Name' ),
        'separate_items_with_commas' => __( 'Separate people categories with commas' ),
        'add_or_remove_items' => __( 'Add or remove people category' ),
        'choose_from_most_used' => __( 'Choose from the most used people categories' ),
        'menu_name' => __( 'People Categories' ),
      ),
      "show_ui" => true, 
      "query_var" => true, 
      'rewrite' => array( 'slug' => 'persons', 'with_front' => true, 'heirarchical' => true )
    )
  );
}

/* 
 * Create_Person_Details information for custom post types 'person'

    @person_title

 *****************************************************/


// next, start creating new form fields 
add_action("admin_init", "register_person_meta");

// register the new section and create a meta box
function register_person_meta() {
  add_meta_box( 'person-meta', 'Person Details', 'setup_person_meta_options', 'person', 'normal', 'high' );
}

// create form 
function setup_person_meta_options() {

  global $post;
  $post_type = $post->post_type;
  $post_parent = $post->post_parent;
  $post_id = $post->ID;

  // first toss any errors to the admin GUI
  $errors = get_transient('settings_errors_file-upload');

  if($errors) {
    // pull the last (aka the first in the array) error from the file-upload settings error
    $my_error = $errors[0]['message'];

    // create a standard admin error
    echo '<div class="error">' . $my_error . '</div>';

    // set the appropriate file upload fieldset background to the standard admin error style
    echo '<style>#file-upload {  background-color: #FFEBE8; border-color: #CC0000; }</style>';

    // just to be clean, remove the error
    delete_transient('settings_errors_file-upload');
  }

// create meta box ONLY if this is a custom post type of 'person' 
  if ( $post_type == 'person') {

    // pull hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    $person_manual_save_flag = get_post_meta($post_id, '_person_manual_save_flag', TRUE);

    // pull form fields

    $person_title = esc_attr( get_post_meta($post_id, '_person_title', TRUE) ); 

// a little CSS love
    echo '<style>'; 
    echo '.person.meta-fields input { width:100%; }';
    echo '.person.meta-fields label { font-weight:bold; font-size:18px; padding:20px; }';
    echo '</style>';
   
// print out a hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    echo '<input type="hidden" name="person_manual_save_flag" value="true" />';

// print out the form fields
    echo "<fieldset class='person meta-fields'><label for='person_title'>Title:</label><input type='text' name='person_title' value='{$person_title}' /><p class='input-description'>The title of the Person</p></fieldset>";

  } // end if ( $post_type == person )
} // end setup_person_meta_options

add_action('save_post', 'save_person_meta', 10, 2);
// save the field data to posts and attachments 
function save_person_meta() {

  global $post;
  $post_type = ''; 
  $post_id = ''; 

  // pull object variables once for later use throughout this function
  if ($post != NULL) {
    $post_type = $post->post_type;
    $post_id = $post->ID;
  } // end if ($post != NULL) 

  // check to see if this is a custom post type of 'person', and the manual save flag exists to ensure this is not the result of an auto-save
  if( $post_type == 'person' && isset($_POST['person_manual_save_flag'])) {

    update_post_meta($post_id, '_person_title', esc_attr($_POST['person_title']));


  }  //end if( $post_type == 'person' && isset($_POST['person_manual_save_flag'])) 

} //end function save_person_meta() 

/* 
 *
 * OUTPUT 
 **************************************************************/

function insert_people( $cat = null, $name = null,  $thumb = false ) {

  // the categories of the person
  $category = $cat;

  // a boolean to show featured image
  $show_thumb = $thumb;

  // this is the slug of a peron and should be a string if populated
  $a_person = $name;

  // empty output variable
  $output = '';

  
  // first checking if this is a single person.  If so, we ignore the category option
  if ( $a_person != null ) {

    $people = get_posts( array( 'post_type' => 'person', 'pagename' => $a_person, 'posts_per_page' => 1 ) );

  // if no single person was indicated and there's no category, then we pull everyone
  } elseif ( $category == null ) {
    
    $people = get_posts( array( 'post_type' => 'person', 'order' => 'ASC', 'orderby' => 'menu_order', 'posts_per_page' => -1 ) );

  // if we get here, then we don't have a single person AND there is a category indicated
  } else {

    $people = get_posts( array( 
      'post_type' => 'person', 
      'tax_query' => array( array (
        'taxonomy' => 'people',
        'field' => 'slug',
        'terms' => $category
      )),
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'posts_per_page' => -1
    ) );

  }

  $cats = '';

  if ( $category != '' ) {
    $cats = ' ' . $category;
  }

  $output .= '<div class="people' . $cats . '">';


  foreach( $people as $person ) {
    
    
    $person_id = $person->ID;

    $person_title = esc_attr( get_post_meta($person_id, '_person_title', TRUE) ); 

    $the_thumb = get_the_post_thumbnail( $person_id, 'full' );
    
    $output .= '<div class="person-item">';
    $output .= '<div class="row">';
    $output .= '<div class="col-md-2">';
    $output .= $the_thumb;
    $output .= '</div><!-- col-md-2 -->';

    $output .= '<div class="col-md-10">';
		$output .= '<header>';
    $output .= '<h4>';
    $output .= $person->post_title; 
    if ( $person_title ) {
    $output .= ', ' . $person_title;
    }
    $output .= '</h4>'; 
    $output .= '</header>';
   
    $output .= '<div class="content">';
    $output .= wpautop($person->post_content); 
    $output .= '</div><!-- .content -->';

    $output .= '</div><!-- .col-md-10 -->';
    $output .= '</div><!-- .row -->';
    $output .= '</div><!-- .person-item -->';

  }
  $output .= '</div><!-- .people -->';


  return $output;

}
/*

SAMPLE USAGE:

to show output of all people with a certain category and their thumbnail
In theme templates:

<?php echo insert_people( 'some-category-slug', 'some-person-slug',  false ); ?>o

*/


/*
 * inner_faq_shortcode() 
 * Allows display of slideshows in the WYSIWYG Editor 
 * USAGE:
 *  create custom field
 *  insert shortcode [bkt-faqs cat=some-faq-category-slug]
 *************************************************/
function insert_people_shortcode($atts) {
  $a = shortcode_atts( array(
    'cat' => '',
    'name' => '',
    'photo' => false
  ), $atts );

  $cat = $a['cat'];
  $name = $a['name'];
  $photo = $a['photo'];

  return insert_people( $cat, $name, $photo );

}

add_shortcode('bkt-people', 'insert_people_shortcode');


?>
