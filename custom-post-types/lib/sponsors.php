<?php

/*
 * Register_custom_post_type - Sponsors 
 * - this custom post type is mainly utilized for when you need to have a sponsor of content embedded
 *   within a page template somewhere, and need a place to manage it from. This allows us to not
 *   put the content inside of a regular Post and have it lost when the blog fills up.
 *************************************************/

add_action( 'init', 'create_sponsor_post_type' );

function create_sponsor_post_type() {

  $icon = plugins_url( 'images/script-code-single.png', __FILE__ ); 

  // create the custom post type
  register_post_type( 'sponsor',
    array(
      'labels' => array(
        'name' => __( 'Sponsors' ), 
        'singular_name' => __( 'Sponsor' ), 
        'add_new' => _x('Add New', 'sponsor'), 
        'add_new_item' => __('Add New Sponsor'), 
        'edit_item' => __('Edit Sponsor'), 
        'new_item' => __('New Sponsor'), 
        'view_item' => __('View Sponsor'), 
        'search_items' => __('Search Sponsors'), 
        'not_found' =>  __('No sponsors found'), 
        'not_found_in_trash' => __('No sponsors found in Trash'), 
        'parent_item_colon' => '', 
        'menu_name' => 'Sponsors'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true, 
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true, 
      'hierarchical' => true,
      'menu_position' => 20, // tosses the menu just below Pages and above Comments
      'menu_icon' => $icon,
      'supports' => array( 'title', 'author', 'editor', 'excerpt', 'page-attributes', 'thumbnail', 'revisions' )
    )
  );

  // create categories for the custom post type
  register_taxonomy(
    "sponsors", 
    array("sponsor"), 
    array(
      "hierarchical" => true, 
      "labels" => array(
        'name' => _x( 'Sponsor Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'Sponsor Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Sponsor Categories' ),
        'popular_items' => __( 'Popular Sponsor Categories' ),
        'all_items' => __( 'All Sponsor Categories' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Sponsor Category' ), 
        'update_item' => __( 'Update Sponsor Category' ),
        'add_new_item' => __( 'Add New Sponsor Category' ),
        'new_item_name' => __( 'New Sponsor Category Name' ),
        'separate_items_with_commas' => __( 'Separate sponsor categories with commas' ),
        'add_or_remove_items' => __( 'Add or remove sponsor category' ),
        'choose_from_most_used' => __( 'Choose from the most used sponsor categories' ),
        'menu_name' => __( 'Sponsor Categories' ),
      ),
      "show_ui" => true, 
      "query_var" => true, 
      'rewrite' => array( 'slug' => 'sponsors', 'with_front' => true, 'heirarchical' => true )
    )
  );
}

if ( function_exists( 'add_image_size' ) ) { 
  add_image_size( 'footer-logo-thumb', 9999, 53 ); //53 pixels tall (and unlimited width)
  add_image_size( 'sidebar-logo-thumb', 230, 9999 ); //230 pixels wide (and unlimited tall)
}

/* add URL text box to sponsors admin page */

// next, start creating new form fields 
add_action("admin_init", "register_sponsor_meta");

// register the new section and create a meta box
function register_sponsor_meta() {
  add_meta_box( 'sponsor-meta', 'Sponsor Details', 'setup_sponsor_meta_options', 'sponsor', 'side', 'high' );
}

// create form 
function setup_sponsor_meta_options() {
  global $post;
  $post_type = $post->post_type;
  $post_parent = $post->post_parent;
  $post_id = $post->ID;

// create meta box ONLY if this is a custom post type of 'sponsor' 
  if ( $post_type == 'sponsor') {

    // pull hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    $sponsor_manual_save_flag = get_post_meta($post_id, '_sponsor_manual_save_flag', TRUE);

    $sponsor_url = esc_attr( get_post_meta($post_id, '_sponsor_url', TRUE) );

  // print out a hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    echo '<input type="hidden" name="sponsor_manual_save_flag" value="true" />';

    echo "<fieldset class='meta-fields'><label for='sponsor_url'>Sponsor URL:</label><input type='text' name='sponsor_url' value='{$sponsor_url}' /><p class='input-description'>The website for the Sponsor.</p><p><em>Be sure this is a complete URL.  It should include the 'http://' part.  An example would be 'http://www.yahoo.com/'</em></p></fieldset>";


  } // end if ( $post_type == 'sponsor')

} // end setup_sponsor_meta_options()

add_action('save_post', 'save_sponsor_meta', 10, 2);

function save_sponsor_meta() {

  global $post;
  $post_type = ''; 
  $post_id = ''; 

  // pull object variables once for later use throughout this function
  if ($post != NULL) {
    $post_type = $post->post_type;
    $post_id = $post->ID;
  }

  if( $post_type == 'sponsor' && isset($_POST['sponsor_manual_save_flag'])) {

    update_post_meta($post_id, '_sponsor_url', $_POST['sponsor_url']);

  } // end if 'sponsor' post type and manual save flag was set

} // end save_sponsor_meta()



/* 
 * function that returns an array of Sponsor objects 
 */
function return_sponsors() {
  
  $args = array(
    'post_type' => 'sponsor',
    'numberposts' => '-1'
  );

  $sponsors = get_posts( $args );

  return $sponsors;
}
/* 
 * function that returns an array of logo image tags
 */
function return_sponsor_logos() {
  
  $args = array(
    'post_type' => 'sponsor',
    'numberposts' => '-1'
  );
  $sponsors = get_posts( $args );
  $logos;
  foreach ( $sponsors as $sponsor ) {
    $logos[] = get_the_post_thumbnail( $sponsor->ID, 'sidebar-logo-thumb' ); 
  }
  return $logos;
}

function return_random_sponsor_logo() {
  $logos = return_sponsor_logos();
  $key = array_rand($logos);
  return $logos[$key];
}

function return_random_logo_with_info() {
  $sponsors = return_sponsors();

  $rand_sponsor_key = array_rand($sponsors);
  $rand_sponsor  = $sponsors[$rand_sponsor_key];
  $rand_sponsor_id = $rand_sponsor->ID;

  $thumbnail = get_the_post_thumbnail( $rand_sponsor_id, 'sidebar-logo-thumb' ); 

  $sponsor_url = get_post_meta( $rand_sponsor_id, '_sponsor_url', TRUE );

  $sponsor_cats = get_the_terms( $rand_sponsor_id, 'sponsors' );
  $sponsor_cat_names = array();
  foreach ( $sponsor_cats as $acat ) {
    $sponsor_cat_names[] = $acat->name;
  }
  $sponsor_category_name = $sponsor_cat_names[0];

  $output;

  return $output;

}

function return_all_sponsors_by_term() {

  $terms = get_terms( 'sponsors' );

  $output = '';

  foreach ( $terms as $term ) {
  
    $output .= '<div class="sponsor-group clearfix">';

    $output .= '<h2>' . $term->name . '</h2>';

    $args = array(
      'post_type' => 'sponsor',
      'numberposts' => '-1',
      'sponsors' => $term->slug
    );

    $these_sponsors = get_posts( $args );

    foreach ( $these_sponsors as $sponsor ) {
      
      // get us an ID
      $sponsor_id = $sponsor->ID;

      // get the sponsors featured image
      $sponsor_thumb = get_the_post_thumbnail( $sponsor_id, array(175,9999) );

      // get the link to the sponsor's website
      $sponsor_url = get_post_meta( $sponsor_id, '_sponsor_url', TRUE );

      // now that we have all that, let's build the output
      $output .= '<div class="sponsor-item"><a href="' . $sponsor_url . '" target="_blank">' . $sponsor_thumb . '</a></div>';

    }

    $output .= '</div><!-- .sponsor-group -->';

  }

  return $output;

}

function return_sponsors_by_term( $atts ) {
  extract( shortcode_atts( array(
    'cat' => 'null'
  ), $atts ) );

    $output = '';
  
    if ( $cat!= 'null' ) {
      
      $cat = esc_attr( $cat );

      $the_term = get_term_by('slug', $cat, 'sponsors');

      $output .= '<div class="sponsor-group clearfix">';

      $output .= '<h2>' . $the_term->name . '</h2>';

      $args = array(
        'post_type' => 'sponsor',
        'numberposts' => '-1',
        'sponsors' => $the_term->slug
      );
      $these_sponsors = get_posts( $args );

      foreach ( $these_sponsors as $sponsor ) {
        
        // get us an ID
        $sponsor_id = $sponsor->ID;

        // get the sponsors featured image
        $sponsor_thumb = get_the_post_thumbnail( $sponsor_id, array(175,9999) );

        // get the link to the sponsor's website
        $sponsor_url = get_post_meta( $sponsor_id, '_sponsor_url', TRUE );

        // now that we have all that, let's build the output
        $output .= '<div class="sponsor-item"><a href="' . $sponsor_url . '" target="_blank">' . $sponsor_thumb . '</a></div>';

      }

      $output .= '</div><!-- .sponsor-group -->';

    } else {

      $output .= return_all_sponsors_by_term();

    }

    return $output;
}
add_shortcode( 'sponsor-category', 'return_sponsors_by_term' );

/**
 * Sponsor_sidebar_widget 
 *************************************************/

class SponsorWidget extends WP_Widget {

/* PREP FOR PULLING DATA AND SETTING OUTPUT
 ******************************************************/


  /** constructor */

  function __construct() {
    $widget_ops = array('description' => 'A sponsor widget that gives you the ability to add a random sponsor to the sidebar.' );
    parent::__construct(false, $name = 'SpnsorWidget', $widget_ops );  
  } // end construct

  /** @see WP_Widget::widget 
  * this is the output to the browser
  * - pull the options out of $args
  * - check to see if empty, then assign them value from $instance
  * - output images and links
  */
  function widget($args, $instance) {   

    // grab the page
    global $wp_query;

    echo $before_widget; 
    //echo $before_title . 'Featured Sponsor' . $after_title;

    ?>
    <aside id="sponsor-widget" class="widget widget_text">
    <h3 class="widget-title">Featured Sponsor</h3>
    <div class="textwidget">
    <?

    $sponsors = return_sponsors();

    $rand_sponsor_key = array_rand($sponsors);
    $rand_sponsor  = $sponsors[$rand_sponsor_key];
    $rand_sponsor_id = $rand_sponsor->ID;

    $thumbnail = get_the_post_thumbnail( $rand_sponsor_id, 'sidebar-logo-thumb' ); 

    $sponsor_url = get_post_meta( $rand_sponsor_id, '_sponsor_url', TRUE );

    $sponsor_cats = get_the_terms( $rand_sponsor_id, 'sponsors' );
    $sponsor_cat_names = array();
    foreach ( $sponsor_cats as $acat ) {
      $sponsor_cat_names[] = $acat->name;
    }
    $sponsor_category_name = $sponsor_cat_names[0];
    ?>
      <div class="sponsor-image"><? echo $thumbnail; ?></div>
      <h2><? echo $sponsor_category_name; ?> Sponsor</h2>
      <a class="more" href="<? echo $sponsor_url; ?>">Visit Website</a>
    </div>
    </aside>
    <?

    echo $after_wdiget;

  } // end widget()

} // end class

add_action( 'widgets_init', create_function( '', 'register_widget( "SponsorWidget" );' ) );

?>
