<?php

/*
 * Register_custom_post_type - Vendors 
 * - Vendor custom post type for 'Business Directory' output
 *************************************************/

add_action( 'init', 'create_vendor_post_type' );

function create_vendor_post_type() {

  $icon = plugins_url( 'images/script-code-single.png', __FILE__ ); 

  // create the custom post type
  register_post_type( 'vendor',
    array(
      'labels' => array(
        'name' => __( 'Vendors' ), 
        'singular_name' => __( 'Vendor' ), 
        'add_new' => _x('Add New', 'vendor'), 
        'add_new_item' => __('Add New Vendor'), 
        'edit_item' => __('Edit Vendor'), 
        'new_item' => __('New Vendor'), 
        'view_item' => __('View Vendor'), 
        'search_items' => __('Search Vendors'), 
        'not_found' =>  __('No vendors found'), 
        'not_found_in_trash' => __('No vendors found in Trash'), 
        'parent_item_colon' => '', 
        'menu_name' => 'Vendors'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true, 
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true, 
      'hierarchical' => true,
      'menu_position' => 20, // tosses the menu just below Pages and above Comments
      'menu_icon' => $icon,
      'supports' => array( 'title', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'page-attributes')
    )
  );

  // create categories for the custom post type
  register_taxonomy(
    "vendors", 
    array("vendor"), 
    array(
      "hierarchical" => true, 
      "labels" => array(
        'name' => _x( 'Vendor Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'Vendor Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Vendor Categories' ),
        'popular_items' => __( 'Popular Vendor Categories' ),
        'all_items' => __( 'All Vendor Categories' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Vendor Category' ), 
        'update_item' => __( 'Update Vendor Category' ),
        'add_new_item' => __( 'Add New Vendor Category' ),
        'new_item_name' => __( 'New Vendor Category Name' ),
        'separate_items_with_commas' => __( 'Separate vendor categories with commas' ),
        'add_or_remove_items' => __( 'Add or remove vendor category' ),
        'choose_from_most_used' => __( 'Choose from the most used vendor categories' ),
        'menu_name' => __( 'Vendor Categories' ),
      ),
      "show_ui" => true, 
      "show_admin_column" => true,
      "query_var" => true, 
      'rewrite' => array( 'slug' => 'vendors', 'with_front' => true, 'heirarchical' => true )
    )
  );
}


/* 
 * Create_Vendor_Details information for custom post types 'vendor'

    @vendor_title
    @vendor_phone
    @vendor_url

 *****************************************************/


// add a specific thumbnail size that only sets the width
/*
if ( function_exists( 'add_image_size' ) ) { 
  add_image_size( 'vendor-thumb', 150, 9999 ); //150 pixels wide (and unlimited height)
}
*/

// first add new enctype to post edit form to handle upload field
/*
function solamar_add_edit_form_multipart_encoding() {
  echo ' enctype="multipart/form-data"';
}
add_action('post_edit_form_tag', 'solamar_add_edit_form_multipart_encoding');
*/


// next, start creating new form fields 
add_action("admin_init", "register_vendor_meta");

// register the new section and create a meta box
function register_vendor_meta() {
  add_meta_box( 'vendor-meta', 'Vendor Details', 'setup_vendor_meta_options', 'vendor', 'normal', 'high' );
}

// create form 
function setup_vendor_meta_options() {

  global $post;
  $post_type = $post->post_type;
  $post_parent = $post->post_parent;
  $post_id = $post->ID;

  // first toss any errors to the admin GUI
  $errors = get_transient('settings_errors_file-upload');

  if($errors) {
    // pull the last (aka the first in the array) error from the file-upload settings error
    $my_error = $errors[0]['message'];

    // create a standard admin error
    echo '<div class="error">' . $my_error . '</div>';

    // set the appropriate file upload fieldset background to the standard admin error style
    echo '<style>#file-upload {  background-color: #FFEBE8; border-color: #CC0000; }</style>';

    // just to be clean, remove the error
    delete_transient('settings_errors_file-upload');
  }

// create meta box ONLY if this is a custom post type of 'vendor' 
  if ( $post_type == 'vendor') {

    // pull hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    $testimonail_manual_save_flag = get_post_meta($post_id, '_vendor_manual_save_flag', TRUE);

    // pull form fields

    $vendor_title = esc_attr( get_post_meta($post_id, '_vendor_title', TRUE) ); 
    $vendor_phone = esc_attr( get_post_meta($post_id, '_vendor_phone', TRUE) ); 
    $vendor_url = esc_attr( get_post_meta($post_id, '_vendor_url', TRUE) ); 

// a little CSS love
    echo '<script type="text/javascript" src="' . WP_PLUGIN_URL . '/solamar-custom/function/js/jquery.tablesorter.js"></script>';
    echo '<style>'; 
    echo '.vendor.meta-fields input { width:100%; }';
    echo '.vendor.meta-fields label { font-weight:bold; font-size:18px; padding:20px; }';
    echo '</style>';
// print out a hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    echo '<input type="hidden" name="vendor_manual_save_flag" value="true" />';

// print out the form fields
    echo "<fieldset class='vendor meta-fields'><label for='vendor_title'>Vendor Title:</label><input type='text' name='vendor_title' value='{$vendor_title}' /><p class='input-description'>The name of the Vendor</p></fieldset>";
    echo "<fieldset class='vendor meta-fields'><label for='vendor_phone'>Vendor Phone:</label><input type='text' name='vendor_phone' value='{$vendor_phone}' /><p class='input-description'>Phone number</p></fieldset>";
    echo "<fieldset class='vendor meta-fields'><label for='vendor_url'>Vendor URL:</label><input type='text' name='vendor_url' value='{$vendor_url}' /><p class='input-description'>The Vendor's website</p></fieldset>";

  } // end if ( $post_type == vendor )
} // end setup_vendor_meta_options

add_action('save_post', 'save_vendor_meta', 10, 2);
// save the field data to posts and attachments 
function save_vendor_meta() {

  global $post;
  $post_type = ''; 
  $post_id = ''; 

  // pull object variables once for later use throughout this function
  if ($post != NULL) {
    $post_type = $post->post_type;
    $post_id = $post->ID;
  } // end if ($post != NULL) 

  // check to see if this is a custom post type of 'vendor', and the manual save flag exists to ensure this is not the result of an auto-save
  if( $post_type == 'vendor' && isset($_POST['vendor_manual_save_flag'])) {

    update_post_meta($post_id, '_vendor_title', esc_attr($_POST['vendor_title']));
    update_post_meta($post_id, '_vendor_phone', esc_attr($_POST['vendor_phone']));
    update_post_meta($post_id, '_vendor_url', esc_attr($_POST['vendor_url']));


  }  //end if( $post_type == 'vendor' && isset($_POST['vendor_manual_save_flag'])) 

} //end function save_vendor_meta() 

/* 
* Set up Display pages
**************************************/

//Template fallback
add_action("template_redirect", 'my_vendor_redirect');

function my_vendor_redirect() {
  global $wp;
  $public_query_vars = $wp->public_query_vars;  
  $plugindir = dirname( __FILE__ );

  //A Specific Custom Post Type
  /*
  echo '<pre>';
  var_dump( $wp );
  echo '</pre>';
  */
  if (isset($wp->query_vars["pagename"]) && $wp->query_vars["pagename"] == 'local-business-finder') {

    $templatefilename = 'page-vendors.php';

    if (file_exists(TEMPLATEPATH . '/' . $templatefilename)) {
      $return_template = TEMPLATEPATH . '/' . $templatefilename;
    } else {
      $return_template = $plugindir . '/themefiles/' . $templatefilename;
    }   
    do_vendor_redirect($return_template);
  }
  if ( is_singular( 'vendor' ) ) {

    $templatefilename = 'single-vendor.php';

    if (file_exists(TEMPLATEPATH . '/' . $templatefilename)) {
      $return_template = TEMPLATEPATH . '/' . $templatefilename;
    } else {
      $return_template = $plugindir . '/themefiles/' . $templatefilename;
    }   

    do_vendor_redirect($return_template);

  }
}

function do_vendor_redirect($url) {

  global $post, $wp_query;

  if (have_posts()) {
    include($url);
    die();
  } else {
    $wp_query->is_404 = true;
  }

}

/* 
 * insert_vendors()
 ***************************************/
function insert_vendors() {

  // output variable
  $output = '';

  // pull the taxonomy so we can sort by term
  $terms = get_terms('vendors');
  foreach ( $terms as $term ) {

    $term_slug = $term->slug;
    $term_title = $term->name;

    /* PULL THE DATA */
    $vendors =  get_posts(
      array(
        'post_type' => 'vendor',
        'vendors' => $term_slug, 
        'order' => 'ASC',
        'orderby' => 'title',
        'posts_per_page' => '-1'
      )
    );

    if( $vendors != NULL ) { // check to see if an object was created successfully above

      $output .= '<h2>' . $term_title . '</h2>';

      foreach ( $vendors as $loop ) {

        $post_id = $loop->ID; 

        $thumb = '';
        $size = 'full';
        if ( has_post_thumbnail( $post_id ) ) {
          $thumb = get_the_post_thumbnail( $post_id, $size );
        }

        $vendor_title = get_post_meta( $post_id, '_vendor_title', TRUE );
        $vendor_phone = get_post_meta( $post_id, '_vendor_phone', TRUE );
        $vendor_url = get_post_meta( $post_id, '_vendor_url', TRUE );

        // clean the URL
        $vendor_url = preg_replace( '#^https?://#', '', $vendor_url );

  /* SET THE OUTPUT */

        $output .= '<table class="vendor-list">';
        $output .= '<tr>';
        $output .= '  <td>' . $thumb . '</td>';
        $output .= '  <td>' . $vendor_title . '</td>';
        $output .= '  <td>' . $vendor_phone . '</td>';

        $output .= '  <td>';
      
        if ( $vendor_url != '' ) { 
          $output .= '    <strong>URL:</strong> <a href="http://' . $vendor_url . '" target="_blank">' . $vendor_url . '</a></td>';
        }
        $output .= '  </td>';
        $output .= '</tr>';


      } // end foreach ( $vendors as $loop )

      $output .= '</table>';

    } else { // if no object is created then return an empty string

      // flush out terms and vendors
      $terms = '';
      $vendors = null;


    }  // end if ( $vendors != null )
  } // end for each $terms

  return $output;
}

?>
