<?php

/*
 * Register_custom_post_type - DJs 
 * - this custom post type is mainly utilized for when you need to have a dj of content embedded
 *   within a page template somewhere, and need a place to manage it from. This allows us to not
 *   put the content inside of a regular Post and have it lost when the blog fills up.
 *************************************************/

add_action( 'init', 'create_dj_post_type' );

function create_dj_post_type() {

  $icon = plugins_url( 'images/script-code-single.png', __FILE__ ); 

  // create the custom post type
  register_post_type( 'dj',
    array(
      'labels' => array(
        'name' => __( 'DJs' ), 
        'singular_name' => __( 'DJ' ), 
        'add_new' => _x('Add New', 'dj'), 
        'add_new_item' => __('Add New DJ'), 
        'edit_item' => __('Edit DJ'), 
        'new_item' => __('New DJ'), 
        'view_item' => __('View DJ'), 
        'search_items' => __('Search DJs'), 
        'not_found' =>  __('No djs found'), 
        'not_found_in_trash' => __('No djs found in Trash'), 
        'parent_item_colon' => '', 
        'menu_name' => 'DJs'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true, 
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true, 
      'hierarchical' => true,
      'menu_position' => 20, // tosses the menu just below Pages and above Comments
      'menu_icon' => $icon,
      'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'page-attributes')
    )
  );

  // create categories for the custom post type
  register_taxonomy(
    "djs", 
    array("dj"), 
    array(
      "hierarchical" => true, 
      "labels" => array(
        'name' => _x( 'DJ Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'DJ Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search DJ Categories' ),
        'popular_items' => __( 'Popular DJ Categories' ),
        'all_items' => __( 'All DJ Categories' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit DJ Category' ), 
        'update_item' => __( 'Update DJ Category' ),
        'add_new_item' => __( 'Add New DJ Category' ),
        'new_item_name' => __( 'New DJ Category Name' ),
        'separate_items_with_commas' => __( 'Separate dj categories with commas' ),
        'add_or_remove_items' => __( 'Add or remove dj category' ),
        'choose_from_most_used' => __( 'Choose from the most used dj categories' ),
        'menu_name' => __( 'DJ Categories' ),
      ),
      "show_ui" => true, 
      "query_var" => true, 
      'rewrite' => array( 'slug' => 'djs', 'with_front' => true, 'heirarchical' => true )
    )
  );
}

/* 
* Set up Display pages
**************************************/

//Template fallback
add_action("template_redirect", 'my_dj_redirect');

function my_dj_redirect() {
  global $wp;
  $public_query_vars = $wp->public_query_vars;  
  $plugindir = dirname( __FILE__ );


  //A Specific Custom Post Type
  /*
  $output .= '<pre>';
  var_dump( $wp );
  $output .= '</pre>';
  if (isset($wp->query_vars["pagename"]) && $wp->query_vars["pagename"] == 'djs') {

    $templatefilename = 'page-on-air.php';

    if (file_exists(TEMPLATEPATH . '/' . $templatefilename)) {
      $return_template = TEMPLATEPATH . '/' . $templatefilename;
    } else {
      $return_template = $plugindir . '/themefiles/' . $templatefilename;
    }   
    do_dj_redirect($return_template);
  }
  */
  if ( is_singular( 'dj' ) ) {

    $templatefilename = 'single-dj.php';

    if (file_exists(TEMPLATEPATH . '/' . $templatefilename)) {
      $return_template = TEMPLATEPATH . '/' . $templatefilename;
    } else {
      $return_template = $plugindir . '/themefiles/' . $templatefilename;
    }   

    do_dj_redirect($return_template);

  }
}

function do_dj_redirect($url) {

  global $post, $wp_query;

  if (have_posts()) {
    include($url);
    die();
  } else {
    $wp_query->is_404 = true;
  }

}

/* 
 * Create_DJ_Details information for custom post types 'dj'

    @dj_day 0 - 6
    @dj_hour 0 - 24
    @dj_minute 00, 30

 *****************************************************/


// next, start creating new form fields 
add_action("admin_init", "register_dj_meta");

// register the new section and create a meta box
function register_dj_meta() {
  add_meta_box( 'dj-meta', 'DJ Details', 'setup_dj_meta_options', 'dj', 'normal', 'high' );
}

// create form 
function setup_dj_meta_options() {

  global $post;
  $post_type = $post->post_type;
  $post_parent = $post->post_parent;
  $post_id = $post->ID;

// create meta box ONLY if this is a custom post type of 'dj' 
  if ( $post_type == 'dj') {

    // pull hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    $testimonail_manual_save_flag = get_post_meta($post_id, '_dj_manual_save_flag', TRUE);

    // pull form fields

    $dj_day_start = esc_attr( get_post_meta($post_id, '_dj_day_start', TRUE) ); 
    $dj_hour_start = esc_attr( get_post_meta($post_id, '_dj_hour_start', TRUE) ); 
    $dj_minute_start = esc_attr( get_post_meta($post_id, '_dj_minute_start', TRUE ) ); 
    $dj_day_end = esc_attr( get_post_meta($post_id, '_dj_day_end', TRUE) ); 
    $dj_hour_end = esc_attr( get_post_meta($post_id, '_dj_hour_end', TRUE) ); 
    $dj_minute_end = esc_attr( get_post_meta($post_id, '_dj_minute_end', TRUE ) ); 

    // weekend slot one
    $dj_day_one_start = esc_attr( get_post_meta($post_id, '_dj_day_one_start', TRUE) ); 
    $dj_hour_one_start = esc_attr( get_post_meta($post_id, '_dj_hour_one_start', TRUE) ); 
    $dj_minute_one_start = esc_attr( get_post_meta($post_id, '_dj_minute_one_start', TRUE ) ); 
    $dj_day_one_end = esc_attr( get_post_meta($post_id, '_dj_day_one_end', TRUE) ); 
    $dj_hour_one_end = esc_attr( get_post_meta($post_id, '_dj_hour_one_end', TRUE) ); 
    $dj_minute_one_end = esc_attr( get_post_meta($post_id, '_dj_minute_one_end', TRUE ) ); 

    // weekend slot two 
    $dj_day_two_start = esc_attr( get_post_meta($post_id, '_dj_day_two_start', TRUE) ); 
    $dj_hour_two_start = esc_attr( get_post_meta($post_id, '_dj_hour_two_start', TRUE) ); 
    $dj_minute_two_start = esc_attr( get_post_meta($post_id, '_dj_minute_two_start', TRUE ) ); 
    $dj_day_two_end = esc_attr( get_post_meta($post_id, '_dj_day_two_end', TRUE) ); 
    $dj_hour_two_end = esc_attr( get_post_meta($post_id, '_dj_hour_two_end', TRUE) ); 
    $dj_minute_two_end = esc_attr( get_post_meta($post_id, '_dj_minute_two_end', TRUE ) ); 

    // weekend slot three 
    $dj_day_three_start = esc_attr( get_post_meta($post_id, '_dj_day_three_start', TRUE) ); 
    $dj_hour_three_start = esc_attr( get_post_meta($post_id, '_dj_hour_three_start', TRUE) ); 
    $dj_minute_three_start = esc_attr( get_post_meta($post_id, '_dj_minute_three_start', TRUE ) ); 
    $dj_day_three_end = esc_attr( get_post_meta($post_id, '_dj_day_three_end', TRUE) ); 
    $dj_hour_three_end = esc_attr( get_post_meta($post_id, '_dj_hour_three_end', TRUE) ); 
    $dj_minute_three_end = esc_attr( get_post_meta($post_id, '_dj_minute_three_end', TRUE ) ); 

    // weekend slot four 
    $dj_day_four_start = esc_attr( get_post_meta($post_id, '_dj_day_four_start', TRUE) ); 
    $dj_hour_four_start = esc_attr( get_post_meta($post_id, '_dj_hour_four_start', TRUE) ); 
    $dj_minute_four_start = esc_attr( get_post_meta($post_id, '_dj_minute_four_start', TRUE ) ); 
    $dj_day_four_end = esc_attr( get_post_meta($post_id, '_dj_day_four_end', TRUE) ); 
    $dj_hour_four_end = esc_attr( get_post_meta($post_id, '_dj_hour_four_end', TRUE) ); 
    $dj_minute_four_end = esc_attr( get_post_meta($post_id, '_dj_minute_four_end', TRUE ) ); 

    // weekend slot five 
    $dj_day_five_start = esc_attr( get_post_meta($post_id, '_dj_day_five_start', TRUE) ); 
    $dj_hour_five_start = esc_attr( get_post_meta($post_id, '_dj_hour_five_start', TRUE) ); 
    $dj_minute_five_start = esc_attr( get_post_meta($post_id, '_dj_minute_five_start', TRUE ) ); 
    $dj_day_five_end = esc_attr( get_post_meta($post_id, '_dj_day_five_end', TRUE) ); 
    $dj_hour_five_end = esc_attr( get_post_meta($post_id, '_dj_hour_five_end', TRUE) ); 
    $dj_minute_five_end = esc_attr( get_post_meta($post_id, '_dj_minute_five_end', TRUE ) ); 

    // create an output variable 

    $output = '';
// print out a hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    $output .= '<input type="hidden" name="dj_manual_save_flag" value="true" />';

    // Monday to Friday
    $output .= '<h2>Monday through Friday</h2>';
    $output .= '<fieldset class="dj mefat-fields">';
    $output .= '<label>start: </label>';
    $output .= '<select name="dj_day_start" id="dj_day_start">';
    $output .= '  <option value="" ' .  selected( $dj_day_start, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="1" ' .  selected( $dj_day_start, '1' , false ) . '>Monday - Friday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_start" id="dj_hour_start">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'midnight';
      }

      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_start, $count , false ) . '>' . $count_display . $ampm . '</option>';

      $count++;

    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_start" id="dj_minute_start">';
    $output .= '  <option value="" ' .  selected( $dj_minute_start, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_start, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_start, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_start, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_start, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '<label style="padding-left:20px;">end :</label> ';
    $output .= '<select name="dj_day_end" id="dj_day_end">';
    $output .= '  <option value="" ' .  selected( $dj_day_end, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="1" ' .  selected( $dj_day_end, '1' , false ) . '>Monday - Friday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_end" id="dj_hour_end">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }

      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_end, $count , false ) . '>' . $count_display . $ampm . '</option>';

      $count++;

    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_end" id="dj_minute_end">';
    $output .= '  <option value="" ' .  selected( $dj_minute_end, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_end, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_end, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_end, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_end, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '</fieldset>';

    $output .= '<hr>';

    // Saturday and Sunday options

    $output .= '<div style="margin:10px 0;">';
    $output .= '<h2>Saturday and Sunday slots</h2>';

    // Day One 
    $output .= '<fieldset class="dj mefat-fields">';
    $output .= '<label>Option 1 start:</label> ';
    $output .= '<select name="dj_day_one_start" id="dj_day_one_start">';
    $output .= '  <option value="" ' .  selected( $dj_day_one_start, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_one_start, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_one_start, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_one_start" id="dj_hour_one_start">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_one_start, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_one_start" id="dj_minute_one_start">';
    $output .= '  <option value="" ' .  selected( $dj_minute_one_start, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_one_start, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_one_start, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_one_start, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_one_start, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '<label style="padding-left:20px;">end :</label> ';
    $output .= '<select name="dj_day_one_end" id="dj_day_one_end">';
    $output .= '  <option value="" ' .  selected( $dj_day_one_end, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_one_end, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_one_end, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_one_end" id="dj_hour_one_end">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_one_end, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_one_end" id="dj_minute_one_end">';
    $output .= '  <option value="" ' .  selected( $dj_minute_one_end, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_one_end, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_one_end, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_one_end, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_one_end, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '</fieldset>';

    $output .= '<hr>';


    // Day Two 
    $output .= '<fieldset class="dj mefat-fields">';
    $output .= '<label>Option 2 start:</label> ';
    $output .= '<select name="dj_day_two_start" id="dj_day_two_start">';
    $output .= '  <option value="" ' .  selected( $dj_day_two_start, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_two_start, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_two_start, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_two_start" id="dj_hour_two_start">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_two_start, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_two_start" id="dj_minute_two_start">';
    $output .= '  <option value="" ' .  selected( $dj_minute_two_start, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_two_start, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_two_start, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_two_start, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_two_start, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '<label style="padding-left:20px;">end :</label> ';
    $output .= '<select name="dj_day_two_end" id="dj_day_two_end">';
    $output .= '  <option value="" ' .  selected( $dj_day_two_end, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_two_end, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_two_end, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_two_end" id="dj_hour_two_end">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_two_end, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_two_end" id="dj_minute_two_end">';
    $output .= '  <option value="" ' .  selected( $dj_minute_two_end, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_two_end, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_two_end, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_two_end, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_two_end, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '</fieldset>';

    $output .= '<hr>';

    // Day Three 
    $output .= '<fieldset class="dj mefat-fields">';
    $output .= '<label>Option 3 start:</label> ';
    $output .= '<select name="dj_day_three_start" id="dj_day_three_start">';
    $output .= '  <option value="" ' .  selected( $dj_day_three_start, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_three_start, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_three_start, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_three_start" id="dj_hour_three_start">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_three_start, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_three_start" id="dj_minute_three_start">';
    $output .= '  <option value="" ' .  selected( $dj_minute_three_start, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_three_start, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_three_start, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_three_start, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_three_start, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '<label style="padding-left:20px;">end :</label> ';
    $output .= '<select name="dj_day_three_end" id="dj_day_three_end">';
    $output .= '  <option value="" ' .  selected( $dj_day_three_end, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_three_end, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_three_end, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_three_end" id="dj_hour_three_end">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_three_end, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_three_end" id="dj_minute_three_end">';
    $output .= '  <option value="" ' .  selected( $dj_minute_three_end, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_three_end, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_three_end, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_three_end, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_three_end, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '</fieldset>';

    $output .= '<hr>';

    // Day Four 
    $output .= '<fieldset class="dj mefat-fields">';
    $output .= '<label>Option 4 start:</label> ';
    $output .= '<select name="dj_day_four_start" id="dj_day_four_start">';
    $output .= '  <option value="" ' .  selected( $dj_day_four_start, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_four_start, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_four_start, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_four_start" id="dj_hour_four_start">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_four_start, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_four_start" id="dj_minute_four_start">';
    $output .= '  <option value="" ' .  selected( $dj_minute_four_start, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_four_start, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_four_start, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_four_start, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_four_start, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '<label style="padding-left:20px;">end :</label> ';
    $output .= '<select name="dj_day_four_end" id="dj_day_four_end">';
    $output .= '  <option value="" ' .  selected( $dj_day_four_end, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_four_end, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_four_end, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_four_end" id="dj_hour_four_end">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_four_end, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_four_end" id="dj_minute_four_end">';
    $output .= '  <option value="" ' .  selected( $dj_minute_four_end, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_four_end, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_four_end, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_four_end, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_four_end, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '</fieldset>';

    $output .= '<hr>';

    // Day Five 
    $output .= '<fieldset class="dj mefat-fields">';
    $output .= '<label>Option 5 start:</label> ';
    $output .= '<select name="dj_day_five_start" id="dj_day_five_start">';
    $output .= '  <option value="" ' .  selected( $dj_day_five_start, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_five_start, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_five_start, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_five_start" id="dj_hour_five_start">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_five_start, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_five_start" id="dj_minute_five_start">';
    $output .= '  <option value="" ' .  selected( $dj_minute_five_start, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_five_start, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_five_start, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_five_start, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_five_start, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '<label style="padding-left:20px;">end :</label> ';
    $output .= '<select name="dj_day_five_end" id="dj_day_five_end">';
    $output .= '  <option value="" ' .  selected( $dj_day_five_end, '' , false ) . '>pick a day</option>';
    $output .= '  <option value="6" ' .  selected( $dj_day_five_end, '6' , false ) . '>Saturday</option>';
    $output .= '  <option value="0" ' .  selected( $dj_day_five_end, '0' , false ) . '>Sunday</option>';
    $output .= '</select>';
    $output .= '<select name="dj_hour_five_end" id="dj_hour_five_end">';
    $hours = 23;
    $count = 0;
    while ( $count < 24 ) {
      $ampm = 'am';
      $count_display = $count;
      if ( $count > 12 ) {
        $ampm = 'pm';
        $count_display = $count - 12;
      }
      if ( $count === 0 ) {
        $count_display = 12;
      }
      if ( $count === 12 ) {
        $ampm = 'noon';
      } elseif ( $count === 24 ) {
        $ampm = 'am';
      }
      $output .= '  <option value="' . $count . '" ' .  selected( $dj_hour_five_end, $count , false ) . '>' . $count_display . $ampm . '</option>';
      $count++;
    }
    $count = 0;
    $output .= '</select>';
    $output .= '<select name="dj_minute_five_end" id="dj_minute_five_end">';
    $output .= '  <option value="" ' .  selected( $dj_minute_five_end, '' , false ) . '>--</option>';
    $output .= '  <option value="0" ' .  selected( $dj_minute_five_end, '0' , false ) . '>00</option>';
    $output .= '  <option value=".25" ' .  selected( $dj_minute_five_end, '.25' , false ) . '>15</option>';
    $output .= '  <option value=".5" ' . selected( $dj_minute_five_end, '.5' , false ) . '>30</option>';
    $output .= '  <option value=".75" ' . selected( $dj_minute_five_end, '.75' , false ) . '>45</option>';
    $output .= '</select>';
    $output .= '</fieldset>';
    $output .= '</div> <!-- style="margin:10px 0;" -->';

    echo $output;

  } // end if ( $post_type == dj )
} // end setup_dj_meta_options

add_action('save_post', 'save_dj_meta', 10, 2);
// save the field data to posts and attachments 
function save_dj_meta() {

  global $post;
  $post_type = ''; 
  $post_id = ''; 

  // pull object variables once for later use throughout this function
  if ($post != NULL) {
    $post_type = $post->post_type;
    $post_id = $post->ID;
  } // end if ($post != NULL) 

  // check to see if this is a custom post type of 'dj', and the manual save flag exists to ensure this is not the result of an auto-save
  if( $post_type == 'dj' && isset($_POST['dj_manual_save_flag'])) {

    update_post_meta($post_id, '_dj_day_start', esc_attr($_POST['dj_day_start']));
    update_post_meta($post_id, '_dj_hour_start', esc_attr($_POST['dj_hour_start']));
    update_post_meta($post_id, '_dj_minute_start', esc_attr($_POST['dj_minute_start']));
    update_post_meta($post_id, '_dj_day_end', esc_attr($_POST['dj_day_end']));
    update_post_meta($post_id, '_dj_hour_end', esc_attr($_POST['dj_hour_end']));
    update_post_meta($post_id, '_dj_minute_end', esc_attr($_POST['dj_minute_end']));

    update_post_meta($post_id, '_dj_day_one_start', esc_attr($_POST['dj_day_one_start']));
    update_post_meta($post_id, '_dj_hour_one_start', esc_attr($_POST['dj_hour_one_start']));
    update_post_meta($post_id, '_dj_minute_one_start', esc_attr($_POST['dj_minute_one_start']));
    update_post_meta($post_id, '_dj_day_one_end', esc_attr($_POST['dj_day_one_end']));
    update_post_meta($post_id, '_dj_hour_one_end', esc_attr($_POST['dj_hour_one_end']));
    update_post_meta($post_id, '_dj_minute_one_end', esc_attr($_POST['dj_minute_one_end']));

    update_post_meta($post_id, '_dj_day_two_start', esc_attr($_POST['dj_day_two_start']));
    update_post_meta($post_id, '_dj_hour_two_start', esc_attr($_POST['dj_hour_two_start']));
    update_post_meta($post_id, '_dj_minute_two_start', esc_attr($_POST['dj_minute_two_start']));
    update_post_meta($post_id, '_dj_day_two_end', esc_attr($_POST['dj_day_two_end']));
    update_post_meta($post_id, '_dj_hour_two_end', esc_attr($_POST['dj_hour_two_end']));
    update_post_meta($post_id, '_dj_minute_two_end', esc_attr($_POST['dj_minute_two_end']));

    update_post_meta($post_id, '_dj_day_three_start', esc_attr($_POST['dj_day_three_start']));
    update_post_meta($post_id, '_dj_hour_three_start', esc_attr($_POST['dj_hour_three_start']));
    update_post_meta($post_id, '_dj_minute_three_start', esc_attr($_POST['dj_minute_three_start']));
    update_post_meta($post_id, '_dj_day_three_end', esc_attr($_POST['dj_day_three_end']));
    update_post_meta($post_id, '_dj_hour_three_end', esc_attr($_POST['dj_hour_three_end']));
    update_post_meta($post_id, '_dj_minute_three_end', esc_attr($_POST['dj_minute_three_end']));

    update_post_meta($post_id, '_dj_day_four_start', esc_attr($_POST['dj_day_four_start']));
    update_post_meta($post_id, '_dj_hour_four_start', esc_attr($_POST['dj_hour_four_start']));
    update_post_meta($post_id, '_dj_minute_four_start', esc_attr($_POST['dj_minute_four_start']));
    update_post_meta($post_id, '_dj_day_four_end', esc_attr($_POST['dj_day_four_end']));
    update_post_meta($post_id, '_dj_hour_four_end', esc_attr($_POST['dj_hour_four_end']));
    update_post_meta($post_id, '_dj_minute_four_end', esc_attr($_POST['dj_minute_four_end']));

    update_post_meta($post_id, '_dj_day_five_start', esc_attr($_POST['dj_day_five_start']));
    update_post_meta($post_id, '_dj_hour_five_start', esc_attr($_POST['dj_hour_five_start']));
    update_post_meta($post_id, '_dj_minute_five_start', esc_attr($_POST['dj_minute_five_start']));
    update_post_meta($post_id, '_dj_day_five_end', esc_attr($_POST['dj_day_five_end']));
    update_post_meta($post_id, '_dj_hour_five_end', esc_attr($_POST['dj_hour_five_end']));
    update_post_meta($post_id, '_dj_minute_five_end', esc_attr($_POST['dj_minute_five_end']));


  }  //end if( $post_type == 'dj' && isset($_POST['dj_manual_save_flag'])) 

} //end function save_dj_meta() 


/* 
 * Display current DJ in the air and DJs coming up
 */
function insert_current_dj() {

  $output = '';
  $output .= '<div class="djs-on-the-air clearfix">';

  $output_current = '';
  $output_next = '';

  $onnow = '<h3 class="on-now">On the air now</h3>';
  $comingup = '<h3 class="coming-up">Coming up</h3>';


  // pull current time and set to Eastern time zone
  $current_hour = idate('H') - 5;
  $current_minute = idate('i') / 60;

  // pull day
  $current_day = idate('w');

  // put together the current time
  $current_time = $current_hour +  $current_minute;

  /* ****************************************************************** begin testing */

  /*
  $current_time = 11.15;
  $current_day = 6; 
  $next_time = $current_time + .45;
  $output .= '<br>today is ' . $current_day . ' and the time is . ' . $current_time . '<br>';
  */

  /* ***************************************************************** end testing */

  // this is to cut off the 'coming up' displays so that we don't see the entire list of DJs in 'coming up'
  $not_too_far = $current_time + 4;

  // this is for the 'coming up' displays for DJs
  $next_time = $current_hour +  $current_minute + 45;

  // set some boolean for testing if we are on a weekday or Saturday or Sunday
  $weekday = false;
  $saturday = false;
  $sunday = false;
  $show_default_message = true;

  // test to see what day we are on and update the above booleans
  if ( $current_day >= 1 && $current_day <= 5) {
    $weekday = true;
  } elseif ( $current_day == 6 ) {
    $saturday = true;
  } elseif ( $current_day == 0 ) {
    $sunday = true;
  }

  // pull all the DJs
  $djs = get_posts( array(
    'post_type' => 'dj',
    'posts_per_page' => -1
  ));

  // if it is Monday  - Friday
  if ( $weekday ) {

    // boolean used to decide whether or not do display the come back later message

    foreach ( $djs as $dj ) {

      // Pull DJ data

      $post_id = $dj->ID;

      $dj_day_start = esc_attr( get_post_meta($post_id, '_dj_day_start', TRUE) ); 
      $dj_hour_start = esc_attr( get_post_meta($post_id, '_dj_hour_start', TRUE) ); 
      $dj_minute_start = esc_attr( get_post_meta($post_id, '_dj_minute_start', TRUE ) ); 

      $dj_day_end = esc_attr( get_post_meta($post_id, '_dj_day_end', TRUE) ); 
      $dj_hour_end = esc_attr( get_post_meta($post_id, '_dj_hour_end', TRUE) ); 
      $dj_minute_end = esc_attr( get_post_meta($post_id, '_dj_minute_end', TRUE ) ); 

      $day = $dj_day_start;

      $starth = floatVal($dj_hour_start);
      $startm = floatVal($dj_minute_start);
      $start = $starth + $startm;

      $endh = floatVal($dj_hour_end);
      $endm = floatVal($dj_minute_end);
      $end = $endh + $endm;

      // the DJ output
      $dj_output = '';
      $thumb = '';
      if ( has_post_thumbnail( $post_id ) ) {
        $thumb = get_the_post_thumbnail( $post_id, 'thumbnail' );
      }
      $dj_output .= '<h3>' . $dj->post_title . '</h3>';
      if ( has_post_thumbnail( $post_id ) ) {
        $dj_output .= $thumb;
      }

      $current_id = '';

      // current DJs
      if ( day_check($dj_day_start, 1) && $current_time >= $start && $current_time <= $end ) {
        $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_start, $dj_minute_start,$dj_hour_end, $dj_minute_end) . '</div></div>';
        $current_id = $post_id;
        $show_default_message = false;
      }
      // up next DJs
      if ( day_check($dj_day_start, 1) && $current_time <= $end && $end < $not_too_far && $current_id != $post_id ) {
        $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_start, $dj_minute_start,$dj_hour_end, $dj_minute_end) . '</div></div>';
        $show_default_message = false;
      }
    } // end foreach djs

  } 
  if ( $weekday && $show_default_message ) {
  }

  if ( $saturday || $sunday ) {

    foreach ( $djs as $dj ) {

      // Pull DJ data

      $post_id = $dj->ID;

      // Day One Data
      $dj_day_one_start = esc_attr( get_post_meta($post_id, '_dj_day_one_start', TRUE) ); 
      $dj_hour_one_start = esc_attr( get_post_meta($post_id, '_dj_hour_one_start', TRUE) ); 
      $dj_minute_one_start = esc_attr( get_post_meta($post_id, '_dj_minute_one_start', TRUE ) ); 

      $dj_day_one_end = esc_attr( get_post_meta($post_id, '_dj_day_one_end', TRUE) ); 
      $dj_hour_one_end = esc_attr( get_post_meta($post_id, '_dj_hour_one_end', TRUE) ); 
      $dj_minute_one_end = esc_attr( get_post_meta($post_id, '_dj_minute_one_end', TRUE ) ); 

      $day_one = $dj_day_one_start;
      $start_one = floatVal($dj_hour_one_start) + floatVal($dj_minute_one_start);
      $end_one = floatVal($dj_hour_one_end) + floatVal($dj_minute_one_end);


      // Day Two Data
      $dj_day_two_start = esc_attr( get_post_meta($post_id, '_dj_day_two_start', TRUE) ); 
      $dj_hour_two_start = esc_attr( get_post_meta($post_id, '_dj_hour_two_start', TRUE) ); 
      $dj_minute_two_start = esc_attr( get_post_meta($post_id, '_dj_minute_two_start', TRUE ) ); 

      $dj_day_two_end = esc_attr( get_post_meta($post_id, '_dj_day_two_end', TRUE) ); 
      $dj_hour_two_end = esc_attr( get_post_meta($post_id, '_dj_hour_two_end', TRUE) ); 
      $dj_minute_two_end = esc_attr( get_post_meta($post_id, '_dj_minute_two_end', TRUE ) ); 

      $day_two = $dj_day_two_start;
      $start_two = floatVal($dj_hour_two_start) + floatVal($dj_minute_two_start);
      $end_two = floatVal($dj_hour_two_end) + floatVal($dj_minute_two_end);

      // Day Three Data
      $dj_day_three_start = esc_attr( get_post_meta($post_id, '_dj_day_three_start', TRUE) ); 
      $dj_hour_three_start = esc_attr( get_post_meta($post_id, '_dj_hour_three_start', TRUE) ); 
      $dj_minute_three_start = esc_attr( get_post_meta($post_id, '_dj_minute_three_start', TRUE ) ); 

      $dj_day_three_end = esc_attr( get_post_meta($post_id, '_dj_day_three_end', TRUE) ); 
      $dj_hour_three_end = esc_attr( get_post_meta($post_id, '_dj_hour_three_end', TRUE) ); 
      $dj_minute_three_end = esc_attr( get_post_meta($post_id, '_dj_minute_three_end', TRUE ) ); 

      $day_three = $dj_day_three_start;
      $start_three = floatVal($dj_hour_three_start) + floatVal($dj_minute_three_start);
      $end_three = floatVal($dj_hour_three_end) + floatVal($dj_minute_three_end);

      // Day Four Data
      $dj_day_four_start = esc_attr( get_post_meta($post_id, '_dj_day_four_start', TRUE) ); 
      $dj_hour_four_start = esc_attr( get_post_meta($post_id, '_dj_hour_four_start', TRUE) ); 
      $dj_minute_four_start = esc_attr( get_post_meta($post_id, '_dj_minute_four_start', TRUE ) ); 

      $dj_day_four_end = esc_attr( get_post_meta($post_id, '_dj_day_four_end', TRUE) ); 
      $dj_hour_four_end = esc_attr( get_post_meta($post_id, '_dj_hour_four_end', TRUE) ); 
      $dj_minute_four_end = esc_attr( get_post_meta($post_id, '_dj_minute_four_end', TRUE ) ); 

      $day_four = $dj_day_four_start;
      $start_four = floatVal($dj_hour_four_start) + floatVal($dj_minute_four_start);
      $end_four = floatVal($dj_hour_four_end) + floatVal($dj_minute_four_end);

      // Day Five Data
      $dj_day_five_start = esc_attr( get_post_meta($post_id, '_dj_day_five_start', TRUE) ); 
      $dj_hour_five_start = esc_attr( get_post_meta($post_id, '_dj_hour_five_start', TRUE) ); 
      $dj_minute_five_start = esc_attr( get_post_meta($post_id, '_dj_minute_five_start', TRUE ) ); 

      $dj_day_five_end = esc_attr( get_post_meta($post_id, '_dj_day_five_end', TRUE) ); 
      $dj_hour_five_end = esc_attr( get_post_meta($post_id, '_dj_hour_five_end', TRUE) ); 
      $dj_minute_five_end = esc_attr( get_post_meta($post_id, '_dj_minute_five_end', TRUE ) ); 

      $day_five = $dj_day_five_start;
      $start_five = floatVal($dj_hour_five_start) + floatVal($dj_minute_five_start);
      $end_five = floatVal($dj_hour_five_end) + floatVal($dj_minute_five_end);

      // the DJ output
      $dj_output = '';
      $thumb = '';
      if ( has_post_thumbnail( $post_id ) ) {
        $thumb = get_the_post_thumbnail( $post_id, 'thumbnail' );
      }
      $dj_output .= '<h3>' . $dj->post_title . '</h3>';
      if ( has_post_thumbnail( $post_id ) ) {
        $dj_output .= $thumb;
      }

      // used to prevent doubling up on DJs
      $current_id = '';

      if ( $saturday ) {
        // current DJs
        if ( day_check($day_one, 6) && $current_time >= $start_one && $current_time <= $end_one ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_one_start, $dj_minute_one_start,$dj_hour_one_end, $dj_minute_one_end) . '</div></div>';
          $current_id = $post_id;
        } elseif ( day_check($day_two, 6) && $current_time >= $start_two && $current_time <= $end_two ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_two_start, $dj_minute_two_start,$dj_hour_two_end, $dj_minute_two_end) . '</div></div>';
          $current_id = $post_id;
        } elseif ( day_check($day_three, 6) && $current_time >= $start_three && $current_time <= $end_three ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_three_start, $dj_minute_three_start,$dj_hour_three_end, $dj_minute_three_end) . '</div></div>';
          $current_id = $post_id;
        } elseif ( day_check($day_four, 6) && $current_time >= $start_four && $current_time <= $end_four ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_four_start, $dj_minute_four_start,$dj_hour_four_end, $dj_minute_four_end) . '</div></div>';
          $current_id = $post_id;
        } elseif ( day_check($day_five, 6) && $current_time >= $start_five && $current_time <= $end_five ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_five_start, $dj_minute_five_start,$dj_hour_five_end, $dj_minute_five_end) . '</div></div>';
          $current_id = $post_id;
        }
        // up next DJs
        if ( day_check($day_one, 6) && $current_time <= $end_one && $end_one < $not_too_far && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_one_start, $dj_minute_one_start,$dj_hour_one_end, $dj_minute_one_end) . '</div></div>';
        } elseif ( day_check($day_two, 6) && $current_time <= $end_two && $end_two < $not_too_far  && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_two_start, $dj_minute_two_start,$dj_hour_two_end, $dj_minute_two_end) . '</div></div>';
        } elseif ( day_check($day_three, 6) && $current_time <= $end_three && $end_three < $not_too_far && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_three_start, $dj_minute_three_start,$dj_hour_three_end, $dj_minute_three_end) . '</div></div>';
        } elseif ( day_check($day_four, 6) && $current_time <= $end_four && $end_four < $not_too_far && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_four_start, $dj_minute_four_start,$dj_hour_four_end, $dj_minute_four_end) . '</div></div>';
        } elseif ( day_check($day_five, 6) && $current_time <= $end_five && $end_five < $not_too_far && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_five_start, $dj_minute_five_start,$dj_hour_five_end, $dj_minute_five_end) . '</div></div>';
        }
      }
      if ( $sunday ) {
        // checking for Sundays
        // current DJs
        if ( day_check($day_one, 0) && $current_time >= $start_one && $current_time <= $end_one ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_one_start, $dj_minute_one_start,$dj_hour_one_end, $dj_minute_one_end) . '</div></div>';
          $current_id = $post_id;
        } elseif ( day_check($day_two, 0) && $current_time >= $start_two && $current_time <= $end_two ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_two_start, $dj_minute_two_start,$dj_hour_two_end, $dj_minute_two_end) . '</div></div>';
          $current_id = $post_id;
        } elseif ( day_check($day_three, 0) && $current_time >= $start_three && $current_time <= $end_three ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_three_start, $dj_minute_three_start,$dj_hour_three_end, $dj_minute_three_end) . '</div></div>';
          $current_id = $post_id;
        } elseif ( day_check($day_four, 0) && $current_time >= $start_four && $current_time <= $end_four ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_four_start, $dj_minute_four_start,$dj_hour_four_end, $dj_minute_four_end) . '</div></div>';
          $current_id = $post_id;
        } elseif ( day_check($day_five, 0) && $current_time >= $start_five && $current_time <= $end_five ) {
          $output_current .= '<div class="dj-on-air">' . $onnow . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_five_start, $dj_minute_five_start,$dj_hour_five_end, $dj_minute_five_end) . '</div></div>';
          $current_id = $post_id;
        }

        // up next DJs
        if ( day_check($day_one, 0) && $current_time <= $end_one && $end_one < $not_too_far && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_one_start, $dj_minute_one_start,$dj_hour_one_end, $dj_minute_one_end) . '</div></div>';
        } elseif ( day_check($day_two, 0) && $current_time <= $end_two && $end_two < $not_too_far && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_two_start, $dj_minute_two_start,$dj_hour_two_end, $dj_minute_two_end) . '</div></div>';
        } elseif ( day_check($day_three, 0) && $current_time <= $end_three && $end_three < $not_too_far && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_three_start, $dj_minute_three_start,$dj_hour_three_end, $dj_minute_three_end) . '</div></div>';
        } elseif ( day_check($day_four, 0) && $current_time <= $end_four && $end_four < $not_too_far && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_four_start, $dj_minute_four_start,$dj_hour_four_end, $dj_minute_four_end) . '</div></div>';
        } elseif ( day_check($day_five, 0) && $current_time <= $end_five && $end_five < $not_too_far && $current_id != $post_id ) {
          $output_next .= '<div class="dj-on-air">' . $comingup . $dj_output . '  <div class="dj-time">' . convert_display_time($dj_hour_five_start, $dj_minute_five_start,$dj_hour_five_end, $dj_minute_five_end) . '</div></div>';
          $show_default_message = false;
        }
      }
      
    } // end foreach


    if ( $show_default_message ) {
      if ( day_check($day_one, 6) ) {
        //$output .= "<h3>We’re live Saturdays from 7am to 2pm. Please check back.</h3>";
      } elseif ( day_check($day_one, 0) ) {
        //$output .= "<h3>We’re live Sundays from 6am to 10pm. Please check back.</h3>";
      }
    }

  } // end if $saturday or $sunday

  $output .= $output_current . $output_next;

  $output .= '</div><!-- end djs-on-the-air -->';

  return $output;
}
/* 
 * convert_display_minute() 
 * converts minutes to human readable display
 * @ $minute - minutes
*************************************************/
function convert_display_minute($minute) {
  $display = $minute * 60;
  if ( $display == 0 ) {
    $display = '00'; 
  }
  return $display;
}
/* 
 * convert_display_time()
 * converts time data collected in DJ editor to human readable isplay
 * @ $hour_start - beginning hour for given day
 * @ $minute_start - beginning minute for given day
 * @ $hour_end - ending hour for given day
 * @ $minute_end - ending minute for given day
*****************************************************************************************/
function convert_display_time($hour_start, $minute_start, $hour_end, $minute_end) {

  $ampm_start = 'am';
  $ampm_end = 'am';
  if ($hour_start > 12) {
    $hour_start = $hour_start - 12;
    $ampm_start = 'pm';
  }
  if ($hour_end > 12) {
    $hour_end = $hour_end - 12;
    $ampm_end = 'pm';
  }
  
  $display_minute_start = convert_display_minute($minute_start);
  $display_minute_end = convert_display_minute($minute_end);

  $display_time = $hour_start . ':' . $display_minute_start . $ampm_start .' to ' . $hour_end . ':' . $display_minute_end . $ampm_end;

  return $display_time;

}
/*
 * day_check()
 * for checking if we are on any given day
 * @ $day - day option set in the DJ admin
 * @ $val - current day of week value - 0 - 6
 ***************************************************/
function day_check($day, $val) {
  if ( $day != null && $day == $val  ) {
    return true;
  } else {
    return false;
  }
}
/* shortcode for DJs currently on the air */
function insert_current_dj_func( $atts ){
  return insert_current_dj();
}
add_shortcode( 'insert-current-dj', 'insert_current_dj_func' );
?>
