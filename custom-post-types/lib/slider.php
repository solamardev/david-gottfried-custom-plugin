<?php

/*
 * Register_custom_post_type - Sliders 
 * - this custom post type is mainly utilized for when you need to have a slider of content embedded
 *   within a page template somewhere, and need a place to manage it from. This allows us to not
 *   put the content inside of a regular Post and have it lost when the blog fills up.
 *************************************************/

add_action( 'init', 'create_slider_post_type' );

function create_slider_post_type() {

  $icon = plugins_url( 'images/script-code-single.png', __FILE__ ); 

  // create the custom post type
  register_post_type( 'slider',
    array(
      'labels' => array(
        'name' => __( 'Sliders' ), 
        'singular_name' => __( 'Slider' ), 
        'add_new' => _x('Add New', 'slider'), 
        'add_new_item' => __('Add New Slider'), 
        'edit_item' => __('Edit Slider'), 
        'new_item' => __('New Slider'), 
        'view_item' => __('View Slider'), 
        'search_items' => __('Search Sliders'), 
        'not_found' =>  __('No sliders found'), 
        'not_found_in_trash' => __('No sliders found in Trash'), 
        'parent_item_colon' => '', 
        'menu_name' => 'Sliders'
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true, 
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true, 
      'hierarchical' => true,
      'menu_position' => 20, // tosses the menu just below Pages and above Comments
      'menu_icon' => $icon,
      'supports' => array( 'title', 'author', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'page-attributes')
    )
  );

  // create categories for the custom post type
  register_taxonomy(
    "sliders", 
    array("slider"), 
    array(
      "hierarchical" => true, 
      "labels" => array(
        'name' => _x( 'Slider Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'Slider Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Slider Categories' ),
        'popular_items' => __( 'Popular Slider Categories' ),
        'all_items' => __( 'All Slider Categories' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Slider Category' ), 
        'update_item' => __( 'Update Slider Category' ),
        'add_new_item' => __( 'Add New Slider Category' ),
        'new_item_name' => __( 'New Slider Category Name' ),
        'separate_items_with_commas' => __( 'Separate slider categories with commas' ),
        'add_or_remove_items' => __( 'Add or remove slider category' ),
        'choose_from_most_used' => __( 'Choose from the most used slider categories' ),
        'menu_name' => __( 'Slider Categories' ),
      ),
      "show_ui" => true, 
      "query_var" => true, 
      'rewrite' => array( 'slug' => 'sliders', 'with_front' => true, 'heirarchical' => true )
    )
  );
}

?>
