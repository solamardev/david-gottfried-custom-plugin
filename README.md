=== Solamar Custom Plugin ===
Contributors: Ben Kaplan
Tags: custom fields, custom post types, booleans, inserts 
Requires at least: 3.0
Tested up to: 3.8.1
Stable tag: 1.2.3

Solamar Custom Plugin lets you do all sorts of fun stuff :)

=== Description ===

Solamar Custom Plugin lets you do all sorts of fun stuff :)

Major new features in Solamar Testimonials include:

1. Ability to add dummy content to pages and posts for shell development
2. A preset custom post type, Bits, that can be used to add small sections of editable content anywhere
3. Custom post type, FAQ
4. Custom post type, Slider for slideshows
5. Facebook Open Graph feature to automatically add FB OG meta tags to header
6. Ability to turn Solamar FB OG meta tags on and off in options panel
7. Custom short code to insert custom fields into content

=== Installation ===

Upload the Solamar plugin to your blog, and activate it.

=== Changelog ===

= 1.2.2 =
[+] added new option page for turning custom Facebook Open Graph meta tags on and off
[+] added insert_fbog() - outputs Facebook Open Graph meta tags 
[+] added catch_all_images() to pull any images inside post content


= 1.2.1 =
[*] added download.php to package

= 1.2 =
[*] added dummy content functionality

= 1.1 = 
[+] Add controls for length of time and length of transition to widget
[*] Slight adjustments to CSS to make styles more generic

= 1.0 =
[*] Base plugin with custom post type, shortcode, and widget.
